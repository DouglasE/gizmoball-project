import physics.Geometry;
import physics.Vect;

public class CollisionExtension  extends CollisionDetails{
    private Geometry.VectPair v;
    public CollisionExtension(double t, Vect v, Gizmo hit, Geometry.VectPair vect) {
        super(t, v, hit);
        this.v = vect;
    }
    public Geometry.VectPair getPair(){
        return v;
    }
}
