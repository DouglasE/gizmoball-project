import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class TickListener implements ActionListener {
    private fakeModel model;
    //Need a reference to the StartListener so that we can change the text
    //of the button when we tick, as when the ball is stopped, the button text would not update
    //when ticked
    private JButton startButton;
    private Timer t;
    public TickListener(fakeModel model, JButton startButton,Timer t) {
        this.model = model;
        this.t = t;
        this.startButton = startButton;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        t.stop();
        for(Gizmo g : model.getGizmos()) {
            if(g instanceof LeftFlipper) {
                g.trigger();
            }
            if(g instanceof RightFlipper) {
                g.trigger();
            }
        }
        for(Ball b : model.getBalls()) {
            if (b != null) {
                b.start();
                model.moveBalls();
                b.stop();
                startButton.setText("Start");
            }
        }
    }
}
