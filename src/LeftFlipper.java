
import Exceptions.noSuchConnectionException;
import physics.*;

import java.awt.*;
import java.util.*;
import java.util.List;

public class LeftFlipper implements Gizmo {
    private final String ID;
    private ROTATION rotation;
    private int XCoord;
    private int YCoord;
    private List<String> keyConnections;
    private List<LineSegment> lines;
    private List<Circle> circles;
    private LineSegment WESTLINE;
    private LineSegment EASTLINE;

    private Circle TOPCIRCLE;
    private Circle BOTTOMCIRCLE;
    private boolean NotInMotion;

    private boolean isAtBottom;

    private final int L = 25;
    private List<String> gizmoConnections;

    private LineSegment WESTLINEBACKUP;
    private LineSegment EASTLINEBACKUP;
    private Circle TOPCIRCLEBACKUP;
    private Circle BOTTOMCIRCLEBACKUP;
    private Color color;
    private Color defaultColor;
    private Color triggerColor;

    public LeftFlipper(String ID,int X, int Y) {
        gizmoConnections = new ArrayList<>();
        NotInMotion = true;
        isAtBottom = true;
        color = Color.yellow;
        lines = new ArrayList<>();
        circles = new ArrayList<>();
        keyConnections = new ArrayList<>();
        this.ID = ID;
        rotation = ROTATION.TOPLEFT;
        this.XCoord = X;
        this.YCoord = Y;


        //y + 0.25 L Means we are shifting it DOWN TO THE FLOOR A QUARTER OF AN L

        WESTLINE = new LineSegment(X, Y + 0.25, X, Y + 1.75);
        TOPCIRCLE = new Circle(X + 0.25, Y + 0.25, 0.25);
        EASTLINE = new LineSegment(X + 0.5, Y + 0.25, X + 0.5, Y + 1.75);

        BOTTOMCIRCLE = new Circle(X + 0.25, Y + 1.75, 0.25);


        lines.add(WESTLINE);
        lines.add(EASTLINE);
        circles.add(TOPCIRCLE);
        circles.add(BOTTOMCIRCLE);

        WESTLINEBACKUP = WESTLINE;
        EASTLINEBACKUP = EASTLINE;
        TOPCIRCLEBACKUP = TOPCIRCLE;
        BOTTOMCIRCLEBACKUP = BOTTOMCIRCLE;
    }


    @Override
    public Color getColor() {
        return color;
    }

    public String getID() {
        return ID;
    }

    public void rotateDegree(double rotatedDegree){

        if(rotation == ROTATION.TOPLEFT) {
            if (WESTLINE.angle().sin() >= Math.sin(0.01)) {
                rotateAround(rotatedDegree);
            }
            else {
                NotInMotion = true;
            }
        }
        else if(rotation == ROTATION.TOPRIGHT) {
            if (WESTLINE.angle().sin() >= Math.sin(-1.61)) {
                rotateAround(rotatedDegree);
            }
            else {
                NotInMotion = true;
            }
        }
        else if(rotation == ROTATION.BOTTOMRIGHT) {
            if (WESTLINE.angle().sin() <= Math.sin(-0.01)) {
                rotateAround(rotatedDegree);
            }
            else {
                NotInMotion = true;
            }
        }
        else if(rotation == ROTATION.BOTTOMLEFT) {
            if (WESTLINE.angle().sin() >= Math.sin(-1.61)) {
                rotateAround(rotatedDegree);

            }
            else {
                NotInMotion = true;
            }
        }

    }

    public void rotateBack() {

        if(rotation == ROTATION.TOPLEFT) {
            if (WESTLINE.angle().sin() <= Math.sin(1.61)) {
                rotateAround(3);
            }
            else {
                NotInMotion = true;
            }
        }
        if(rotation == ROTATION.TOPRIGHT) {
            if (WESTLINE.angle().sin() <= Math.sin(-0.01)) {
                rotateAround(3);
            }
            else {
                NotInMotion = true;
            }
        }
        if(rotation == ROTATION.BOTTOMRIGHT) {
            if (WESTLINE.angle().sin() >= Math.sin(-1.61)) {
                rotateAround(3);
            }
            else {
                NotInMotion = true;
            }
        }
        if(rotation == ROTATION.BOTTOMLEFT) {
            if (WESTLINE.angle().sin() <= Math.sin(-0.01)) {
                rotateAround(3);
            }
            else {
                NotInMotion = true;
            }
        }
    }

    private void rotateAround(double degree) {
        BOTTOMCIRCLE = Geometry.rotateAround(BOTTOMCIRCLE, TOPCIRCLE.getCenter(), new Angle(Math.toRadians(degree)));
        WESTLINE = Geometry.rotateAround(WESTLINE, TOPCIRCLE.getCenter(), new Angle(Math.toRadians(degree)));
        EASTLINE = Geometry.rotateAround(EASTLINE, TOPCIRCLE.getCenter(), new Angle(Math.toRadians(degree)));
        lines.set(0, WESTLINE);
        lines.set(1, EASTLINE);
        circles.set(1, BOTTOMCIRCLE);
        NotInMotion = false;
    }



    @Override
    public void rotate() {

        int X = this.getX();
        int Y = this.getY();
        switch (rotation){
            case TOPRIGHT:
                WESTLINE = new LineSegment(X+1.5,Y+1.75,X+1.5,Y);
                TOPCIRCLE = new Circle(X+1.8,Y+1.75,0.25);
                EASTLINE = new LineSegment(X+2,Y,X+2,Y+1.75);
                BOTTOMCIRCLE = new Circle(X+1.8,Y+0.25,0.25);


                lines.set(0,WESTLINE);
                lines.set(1,EASTLINE);
                circles.set(0,TOPCIRCLE);
                circles.set(1,BOTTOMCIRCLE);

                rotation = ROTATION.BOTTOMRIGHT;

                break;
            case TOPLEFT:
                WESTLINE = new LineSegment(X+0.25, Y+0.25, X+1.75, Y+0.25);
                BOTTOMCIRCLE = new Circle(X+0.25,Y+0.25,0.25);
                EASTLINE = new LineSegment(X+0.25, Y, X+1.75,Y);
                TOPCIRCLE = new Circle(X+1.75, Y+0.25, 0.25);

                lines.set(0,WESTLINE);
                lines.set(1,EASTLINE);
                circles.set(0,TOPCIRCLE);
                circles.set(1,BOTTOMCIRCLE);

                rotation = ROTATION.TOPRIGHT;

                break;
            case BOTTOMRIGHT:
                WESTLINE = new LineSegment(X+0.25,Y+1.5,X+1.75,Y+1.5);
                TOPCIRCLE = new Circle(X+0.25,Y+1.75,0.25);
                EASTLINE = new LineSegment(X+0.25,Y+2,X+1.7,Y+2);
                BOTTOMCIRCLE =new Circle(X+1.8,Y+1.75,0.25);


                lines.set(0,WESTLINE);
                lines.set(1,EASTLINE);
                circles.set(0,TOPCIRCLE);
                circles.set(1,BOTTOMCIRCLE);

                rotation = ROTATION.BOTTOMLEFT;

                break;
            case BOTTOMLEFT:
                WESTLINE = new LineSegment(X, Y + 0.25, X, Y + 1.75);
                TOPCIRCLE = new Circle(X + 0.25, Y + 0.25, 0.25);
                EASTLINE = new LineSegment(X + 0.5, Y + 0.25, X + 0.5, Y + 1.75);
                BOTTOMCIRCLE = new Circle(X + 0.25, Y + 1.75, 0.25);

                lines.set(0,WESTLINE);
                lines.set(1,EASTLINE);
                circles.set(0,TOPCIRCLE);
                circles.set(1,BOTTOMCIRCLE);

                rotation = ROTATION.TOPLEFT;

                break;
            default:
                break;

        }
        EASTLINEBACKUP = EASTLINE;
        WESTLINEBACKUP = WESTLINE;
        TOPCIRCLEBACKUP = TOPCIRCLE;
        BOTTOMCIRCLEBACKUP = BOTTOMCIRCLE;
    }

    @Override
    public int getX() {
        return XCoord;

    }

    @Override
    public int getY() {

        return YCoord;
    }

    @Override
    public String getType() {
        return "LeftFlipper";
    }

    @Override
    public ROTATION getRotation() {
        return rotation;
    }
    public Vect getCenterOfRotation(){
        return TOPCIRCLE.getCenter();
    }
    public boolean currentlyRotating(){
        return !NotInMotion;
    }
    @Override
    public boolean addGizmoConnection(String gizmoToConnect) {
        if(gizmoConnections.contains(gizmoToConnect)){
            return false;
        }
        else{
            return gizmoConnections.add(gizmoToConnect);
        }
    }

    @Override
    public List<String> getGizmoConnections() {
        return gizmoConnections;
    }

    @Override
    public List<LineSegment> getLineSegments() {
        return lines;
    }

    @Override
    public List<Circle> getCircles() {
        return circles;
    }

    @Override
    public int[] getBoardX() {
        return new int[0];
    }

    @Override
    public int[] getBoardY() {
        return new int[0];
    }


    public double[] getBoardXDoouble() {
        return new double[] {circles.get(0).getCenter().x(),circles.get(1).getCenter().x()};
    }


    public double[] getBoardYDouble() {
        return new double[]{ circles.get(0).getCenter().y(),circles.get(1).getCenter().y()};
    }

    @Override
    public boolean addKeyConnect(String key,String UpOrDown) {
        String keyString  = "key" + "." + key + "." + UpOrDown;
        if(!keyConnections.contains(keyString)) {
            keyConnections.add(keyString);
            return true;
        }else{
            return  false;
        }

    }

    @Override
    public List<String> getKeyConnections() {
        return new ArrayList<>(keyConnections);
    }

    @Override
    public void trigger() {

        if(!isAtBottom) {
            rotateDegree(-3);
        }
        else {
            rotateBack();
        }
    }

    @Override
    public void move(int X, int Y) {
        this.XCoord = X;
        this.YCoord = Y;
        WESTLINE = new LineSegment(X, Y + 0.25, X, Y + 1.75);
        TOPCIRCLE = new Circle(X + 0.25, Y + 0.25, 0.25);
        EASTLINE = new LineSegment(X + 0.5, Y + 0.25, X + 0.5, Y + 1.75);
        BOTTOMCIRCLE = new Circle(X + 0.25, Y + 1.75, 0.25);

        WESTLINEBACKUP = WESTLINE;
        EASTLINEBACKUP = EASTLINE;
        TOPCIRCLEBACKUP = TOPCIRCLE;
        BOTTOMCIRCLEBACKUP = BOTTOMCIRCLE;
        lines.set(0,WESTLINE);
        lines.set(1,EASTLINE);
        circles.set(0,TOPCIRCLE);
        circles.set(1,BOTTOMCIRCLE);
        rotate();
        rotate();
        rotate();
        rotate();


    }

    @Override
    public void removeConnection(String remove) throws noSuchConnectionException {
        if(!gizmoConnections.contains(remove)){
            throw new noSuchConnectionException("Connection does not exist!");
        }
        gizmoConnections.remove(remove);

    }

    @Override
    public void removeKeyConnects() {
        keyConnections.clear();
    }

    @Override
    public void reset() {
        NotInMotion = true;
        isAtBottom = true;
        while (rotation != ROTATION.TOPLEFT){
            rotate();
        }
    }

    @Override
    public String toString(){
        StringBuilder s = new StringBuilder("LeftFlipper " + ID + " " + XCoord + " " + YCoord);
        switch (this.getRotation().getNumVal()){
            case 1:
                s.append("\nRotate ").append(ID);

                break;
            case 2:
                break;
            case 3:
                s.append("\nRotate ").append(ID);
                s.append("\nRotate ").append(ID);
                break;
            case 4:
                s.append("\nRotate ").append(ID);
                break;
        }

        for(String keyConnectString : keyConnections){
            String[] strings = keyConnectString.split("\\.",104);
            s.append("\nKeyConnect");
            s.append(" key ").append(strings[1]);
            s.append(" ").append(strings[2]);
            s.append(" ").append(this.getID());

        }
        for(String consumer : gizmoConnections){
            s.append("\nConnect").append(" ").append(this.getID()).append(" ").append(consumer);
        }

        return  s.toString();
    }

    @Override
    public int hashCode(){
        return ID.hashCode();
    }

    public void stateSwap() {
        this.isAtBottom = !this.isAtBottom;
    }

    @Override
    public void setColor(Color color) {
        this.color =color;
    }

    public void resetToBottom() {
        BOTTOMCIRCLE = BOTTOMCIRCLEBACKUP;
        WESTLINE = WESTLINEBACKUP;
        EASTLINE = EASTLINEBACKUP;
        lines.set(0, WESTLINE);
        lines.set(1, EASTLINE);
        circles.set(1, BOTTOMCIRCLE);
        isAtBottom = true;
    }

    public void setInactive() {
        isAtBottom = true;
    }
}
