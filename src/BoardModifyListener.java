import Exceptions.DuplicateGizmoException;
import Exceptions.badSelectException;
import Exceptions.noSuchConnectionException;

import javax.swing.*;
import javax.swing.event.MouseInputListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Vector;

public class BoardModifyListener implements ActionListener, MouseInputListener {
    private fakeModel model;
    private Point[] twopoint;
    private int clickcount;
    private String currentGizmoToPlace;
    private JLabel infoLabel;
    private int absorberX1, absorberY1, absorberX2, absorberY2;
    private List<String> removeList;

    private static final int PIXELS_PER_GRID_BOX = 25;

    public BoardModifyListener(fakeModel model, JLabel infoLabel) {
        this.infoLabel = infoLabel;
        this.model = model;
        twopoint = new Point[1];
        clickcount = 0;
        currentGizmoToPlace = "Square";
        removeList = new ArrayList<>();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equalsIgnoreCase("Clear Board")) {
            model.clear();
            model.setSelectedMode(true);
            infoLabel.setText("Board cleared!");
            infoLabel.setBackground(Color.green);
            infoLabel.setBorder(BorderFactory.createBevelBorder(1));
        } else if (e.getActionCommand().equalsIgnoreCase("Rotate")) {
            model.setRotateMode(true);
            infoLabel.setText("Rotate Mode!");
            infoLabel.setBackground(Color.green);
            infoLabel.setBorder(BorderFactory.createBevelBorder(1));
        } else if (e.getActionCommand().equalsIgnoreCase("Delete")) {

            model.setDeleteMode(true);
            infoLabel.setText("Delete Mode!");
            infoLabel.setBackground(Color.green);
            infoLabel.setBorder(BorderFactory.createBevelBorder(1));
        } else if (e.getActionCommand().equalsIgnoreCase("Move")) {
            clickcount = 0;
            twopoint[0] = null;

            model.setMoveMode(true);
            infoLabel.setText("Move Mode!");
            infoLabel.setBackground(Color.green);
            infoLabel.setBorder(BorderFactory.createBevelBorder(1));
        } else if (e.getActionCommand().equalsIgnoreCase("Connect Gizmo")) {
            clickcount = 0;

            twopoint[0] = null;

            model.setConnectMode(true);
            infoLabel.setText("Connect Mode!");
            infoLabel.setBackground(Color.green);
            infoLabel.setBorder(BorderFactory.createBevelBorder(1));
        } else if (e.getActionCommand().equalsIgnoreCase("Disconnect Gizmo")) {
            clickcount = 0;

            twopoint[0] = null;

            model.setDisconnectMode(true);

            infoLabel.setText("Disconnect Mode!");
            infoLabel.setBackground(Color.green);
            infoLabel.setBorder(BorderFactory.createBevelBorder(1));
        } else if (e.getActionCommand().equalsIgnoreCase("Add Ball")) {

            model.setAddBallMode(true);
            infoLabel.setText("Ready to add/move the ball!");
            infoLabel.setBackground(Color.green);
            infoLabel.setBorder(BorderFactory.createBevelBorder(1));
        } else if (e.getActionCommand().equalsIgnoreCase("comboBoxChanged")) {
            JComboBox jComboBox = (JComboBox) e.getSource();
            currentGizmoToPlace = jComboBox.getSelectedItem().toString();

            //Tooltips
            switch (currentGizmoToPlace) {
                case "Circle":
                    jComboBox.setToolTipText("A circular gizmo that changes colour when activated.");
                    break;
                case "Square":
                    jComboBox.setToolTipText("A square gizmo that changes colour when activated.");
                    break;
                case "Triangle":
                    jComboBox.setToolTipText("A triangular gizmo that changes colour when activated");
                    break;
                case "Absorber":
                    jComboBox.setToolTipText("Upon contact with a ball, an absorber will capture it and hold it until the absorber is triggered, whereupon it fires the ball upwards.");
                    break;
                case "Left flipper":
                    jComboBox.setToolTipText("A pinball-style flipper that rotates when activated. (Left flavour)");
                    break;
                case "Right flipper":
                    jComboBox.setToolTipText("A pinball-style flipper that rotates when activated. (Right flavour)");
                    break;
                case "Teleporter":
                    jComboBox.setToolTipText("A square gizmo that teleports a ball underneath itself when it collides.");
                    break;
            }

            if (model.getAddGizmoMode()) {
                infoLabel.setText("Ready to add a " + currentGizmoToPlace);
                infoLabel.setBackground(Color.green);
                infoLabel.setBorder(BorderFactory.createBevelBorder(1));
            }
        } else if (e.getActionCommand().equalsIgnoreCase("Add Gizmo")) {

            model.setAddGizmoMode(true);
            if (model.getAddGizmoMode()) {
                infoLabel.setText("Ready to add a " + currentGizmoToPlace);
                infoLabel.setBackground(Color.green);
                infoLabel.setBorder(BorderFactory.createBevelBorder(1));
            }
        } else if (e.getActionCommand().equalsIgnoreCase("Disconnect key")) {

            model.setKeyDisconnectMode(true);
            infoLabel.setText("Ready to disconnect a key from a Gizmo!");
            infoLabel.setBackground(Color.green);
            infoLabel.setBorder(BorderFactory.createBevelBorder(1));
        } else if (e.getActionCommand().equalsIgnoreCase("Connect key")) {
            model.setKeyConnectMode(true);
            infoLabel.setText("Ready to connect a key to a Gizmo!");
            infoLabel.setBackground(Color.green);
            infoLabel.setBorder(BorderFactory.createBevelBorder(1));
        } else if (e.getActionCommand().equalsIgnoreCase("Change Colour")) {
            model.setChangeColorMode(true);
            infoLabel.setText("Click the gizmo you wish to change the Colour of!");
            infoLabel.setBackground(Color.green);
            infoLabel.setBorder(BorderFactory.createBevelBorder(1));
        }
        else if (e.getActionCommand().equalsIgnoreCase("Query")){
            model.setQuerymode(true);
            infoLabel.setText("Click the gizmo you wish to query!");
            infoLabel.setBackground(Color.green);
            infoLabel.setBorder(BorderFactory.createBevelBorder(1));
        }

    }

    private void disconnectKey(MouseEvent e) {
        int x, y;
        Point p = e.getPoint();
        x = (int) p.getX() / PIXELS_PER_GRID_BOX;
        y = (int) p.getY() / PIXELS_PER_GRID_BOX;

        try {
            String ID = model.getGizmoAt(x, y);
            if (!ID.isEmpty()) {
                int response = JOptionPane.showConfirmDialog(null, "Are you sure you want to clear key connections to this gizmo?", "Confirm", JOptionPane.YES_NO_OPTION);
                if (response == JOptionPane.YES_OPTION) {
                    model.removeKeyConnect(ID);
                    infoLabel.setText("All Keyconnects removed from: " + ID);
                    infoLabel.setBackground(Color.green);
                    infoLabel.setBorder(BorderFactory.createBevelBorder(1));
                }
            } else {
                infoLabel.setText("Select a gizmo!");
                infoLabel.setBorder(BorderFactory.createBevelBorder(1));
                infoLabel.setBackground(Color.red);
            }


        } catch (badSelectException e1) {
            JOptionPane.showConfirmDialog(null, "Bad selection", "No gizmo there!", JOptionPane.DEFAULT_OPTION);
        }
    }

    private void connectKey(MouseEvent e) {
        int x, y;
        Point p = e.getPoint();
        x = (int) p.getX() / PIXELS_PER_GRID_BOX;
        y = (int) p.getY() / PIXELS_PER_GRID_BOX;

        try {
            String ID = model.getGizmoAt(x, y);
            if (!ID.isEmpty()) {
                JDialog jDialog = new JDialog();
                jDialog.setLayout(new FlowLayout());
                JTextField textField = new JTextField("Press key you want to assign");
                JRadioButton keyDown = new JRadioButton("Key down");
                keyDown.setActionCommand("Key down");
                keyDown.setSelected(true);
                keyDown.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent actionEvent) {
                        jDialog.requestFocus();
                    }
                });
                JRadioButton keyUp = new JRadioButton("Key up");
                keyUp.setActionCommand("Key up");
                keyUp.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent actionEvent) {
                        jDialog.requestFocus();
                    }
                });
                ButtonGroup buttonGroup = new ButtonGroup();
                buttonGroup.add(keyDown);
                buttonGroup.add(keyUp);
                jDialog.add(textField);
                jDialog.add(keyDown);
                jDialog.add(keyUp);
                textField.setEditable(false);
                jDialog.pack();
                jDialog.setLocationRelativeTo(null);
                jDialog.setVisible(true);
                jDialog.setFocusable(true);
                jDialog.requestFocusInWindow();
                jDialog.addKeyListener(new ConnectKeyListener(jDialog, model, ID, infoLabel, buttonGroup));
                jDialog.setModal(true);
            } else {
                infoLabel.setBackground(Color.red);
                infoLabel.setBorder(BorderFactory.createBevelBorder(1));
                infoLabel.setText("Select a gizmo!");
            }
        } catch (badSelectException e1) {
            JOptionPane.showConfirmDialog(null, "Bad selection", "No gizmo there!", JOptionPane.DEFAULT_OPTION);
        }
        model.setSelectedMode(true);
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if (model.getRotateMode()) {
            rotate(e);
        } else if (model.getDeleteMode()) {
            delete(e);
        } else if (model.getMoveMode()) {
            move(e);
        } else if (model.getConnectMode()) {
            connect(e);
        } else if (model.getDisconnectMode()) {
            disconnect(e);
        } else if (model.getAddBallMode()) {
            addBall(e);
        } else if (model.getAddGizmoMode() && !currentGizmoToPlace.equalsIgnoreCase("Absorber")) {
            try {
                addGizmo(e);
            } catch (badSelectException e1) {
                e1.printStackTrace();
            }
        } else if (model.getKeyDisconnectMode()) {
            disconnectKey(e);
        } else if (model.getKeyConnectMode()) {
            connectKey(e);
        } else if (model.getChangeColorMode()) {
            changeColour(e);
        }
        else if(model.getQueryMode()){
            query(e);
        }
    }

    private void query(MouseEvent e) {
        double x, y;
        Point p = e.getPoint();
        x = p.getX() / 25;
        y = p.getY() / 25;


        if (model.getQueryMode()) {
            String ID;
            try {
                ID = model.getGizmoAt(x, y);
                if (!ID.isEmpty()) {
                    Gizmo g = model.getGizmo(ID);

                    if(g!= null){
                        List<String> connections = g.getGizmoConnections();
                        List<String> keyConnections = g.getKeyConnections();
                        JDialog jDialog = new JDialog();
                        JScrollPane scroller = new JScrollPane();
                       Object[] columns = new Object[]{"Connection ID"};
                       //https://stackoverflow.com/questions/22371720/how-to-add-row-dynamically-in-jtable
                      DefaultTableModel dtm = new DefaultTableModel(0, 0){

                          @Override
                          public boolean isCellEditable(int row, int column) {
                              //all cells false
                              return false;
                          }
                      };
                      dtm.setColumnIdentifiers(columns);
                        JTable table = new JTable( );
                        table.setModel(dtm);
                        if(!connections.isEmpty()) {
                          for (String conn : connections) {
                            dtm.addRow(new String[] {conn});
                          }
                        }
                        else {
                          dtm.addRow(new String[] {"No connections!"});
                        }


                      DefaultTableModel dtm2 = new DefaultTableModel(0, 0){

                          @Override
                          public boolean isCellEditable(int row, int column) {
                              //all cells false
                              return false;
                          }
                      };

                      Object[] columns2 = new Object[]{"Key Connection","Up Or Down"};
                      dtm2.setColumnIdentifiers(columns2);
                      JTable table2 = new JTable( );
                      table2.setModel(dtm2);
                      if(!keyConnections.isEmpty()) {
                        for (String conn : keyConnections) {
                          String[] strings = conn.split("\\.",104);
                         KeyEvent.getKeyText(Integer.parseInt(strings[1]));
                         String keytext = KeyEvent.getKeyText(Integer.parseInt(strings[1]));
                          dtm2.addRow(new String[] {keytext,strings[2]});
                        }
                      }
                      else {
                        dtm2.addRow(new String[] {"No key connections!","No key connections!"});
                      }
                        scroller.add(table);
                        scroller.setViewportView(table);

                        JScrollPane scroller2 = new JScrollPane();
                        scroller2.add(table2);
                        scroller2.setViewportView(table2);
                       jDialog.setLayout(new FlowLayout());

                        jDialog.add(scroller,BorderLayout.WEST);
                        jDialog.add(scroller2, BorderLayout.EAST);

                        jDialog.setModal(true);
                        jDialog.pack();
                        jDialog.setLocationRelativeTo(null);
                        jDialog.setVisible(true);
                        jDialog.setFocusable(true);
                        jDialog.requestFocusInWindow();

                    }
                }else{
                    infoLabel.setText("Bad click!");
                    infoLabel.setBackground(Color.red);
                    infoLabel.setBorder(BorderFactory.createBevelBorder(1));
                }



            } catch (badSelectException e1) {
                JOptionPane.showConfirmDialog(null, "Bad selection", "No gizmo there!", JOptionPane.DEFAULT_OPTION);
            }
        }

    }

    private void changeColour(MouseEvent e) {
        double x, y;
        Point p = e.getPoint();
        x = p.getX() / 25;
        y = p.getY() / 25;

        if (model.getChangeColorMode()) {
            String ID;
            try {
                ID = model.getGizmoAt(x, y);
                if (ID.equalsIgnoreCase("")) {
                    infoLabel.setText("You didn't click a gizmo!");
                    infoLabel.setBackground(Color.red);
                    infoLabel.setBorder(BorderFactory.createBevelBorder(1));
                    return;
                }
                Color c = JColorChooser.showDialog(null, "Choose Colour!", Color.black);
                if (c == null) {
                    return;
                }
                model.setGizmoColour(ID, new Color(c.getRGB()));
                infoLabel.setText("Colour change success!");
                infoLabel.setBackground(Color.green);
                infoLabel.setBorder(BorderFactory.createBevelBorder(1));
            } catch (badSelectException e1) {
                JOptionPane.showConfirmDialog(null, "Bad selection", "No gizmo there!", JOptionPane.DEFAULT_OPTION);
            }
        }


    }

    private boolean isSpaceAvailable(MouseEvent e, String gizmo, String movingGizmoId) throws badSelectException {

        if (gizmo.equalsIgnoreCase("Circle")) {
            return model.getGizmoAt(e.getX() / PIXELS_PER_GRID_BOX, e.getY() / PIXELS_PER_GRID_BOX).equals("");
        } else if (gizmo.equalsIgnoreCase("Square")) {
            return model.getGizmoAt(e.getX() / PIXELS_PER_GRID_BOX, e.getY() / PIXELS_PER_GRID_BOX).equals("");

        } else if (gizmo.equalsIgnoreCase("Triangle")) {
            return model.getGizmoAt(e.getX() / PIXELS_PER_GRID_BOX, e.getY() / PIXELS_PER_GRID_BOX).equals("");

        } else if (gizmo.equalsIgnoreCase("Absorber")) {
            boolean available = true;

            if ((e.getY() / PIXELS_PER_GRID_BOX) < 1) {
                available = false;
            } else {
                for (int x = e.getX() / PIXELS_PER_GRID_BOX; x < e.getX() / PIXELS_PER_GRID_BOX + Math.max(absorberX1, absorberX2) - Math.min(absorberX1, absorberX2); x++) {
                    for (int y = e.getY() / PIXELS_PER_GRID_BOX; y < e.getY() / PIXELS_PER_GRID_BOX + Math.max(absorberY1, absorberY2) - Math.min(absorberY1, absorberY2); y++) {
                        if (!model.getGizmoAt(x, y).equals("") && !model.getGizmoAt(x, y).equalsIgnoreCase(movingGizmoId)) {
                            available = false;
                            break;
                        }
                    }

                    if (!available) {
                        break;
                    }
                }

                if (!movingGizmoId.equals("")) {
                    if (e.getX() / PIXELS_PER_GRID_BOX + (Math.max(absorberX1, absorberX2) - Math.min(absorberX1, absorberX2)) > 20 || e.getY() / PIXELS_PER_GRID_BOX + (Math.max(absorberY1, absorberY2) - Math.min(absorberY1, absorberY2)) > 20) {
                        available = false;
                    }
                }
            }
            return available;


        } else if (gizmo.equalsIgnoreCase("LeftFlipper")) {

            boolean available = true;
            for (int x = e.getX() / PIXELS_PER_GRID_BOX; x < (e.getX() / PIXELS_PER_GRID_BOX) + 2; x++) {
                for (int y = e.getY() / PIXELS_PER_GRID_BOX; y < (e.getY() / PIXELS_PER_GRID_BOX + 2); y++) {
                    if (!model.getGizmoAt(x, y).equals("") && !model.getGizmoAt(x, y).equalsIgnoreCase(movingGizmoId)) {
                        System.out.println(model.getGizmoAt(x, y));
                        available = false;
                    }
                }
            }

            return available;

        } else if (gizmo.equalsIgnoreCase("RightFlipper")) {

            boolean available = true;
            for (int x = e.getX() / PIXELS_PER_GRID_BOX; x < (e.getX() / PIXELS_PER_GRID_BOX) + 2; x++) {
                for (int y = e.getY() / PIXELS_PER_GRID_BOX; y < (e.getY() / PIXELS_PER_GRID_BOX + 2); y++) {
                    if (!model.getGizmoAt(x, y).equals("") && !model.getGizmoAt(x, y).equalsIgnoreCase(movingGizmoId)) {
                        available = false;
                    }
                }
            }

            return available;

        } else if (gizmo.equalsIgnoreCase("Teleporter")) {

            String gizmoAt1 = model.getGizmoAt(e.getX() / PIXELS_PER_GRID_BOX, e.getY() / PIXELS_PER_GRID_BOX);
            String gizmoAt2 = model.getGizmoAt(e.getX() / PIXELS_PER_GRID_BOX, e.getY() / PIXELS_PER_GRID_BOX + 1);
            return (gizmoAt1.equals("") || gizmoAt1.contains("TP")) && (gizmoAt2.equals("") || gizmoAt2.contains("TP"));
        }


        return false;
    }


    private void addGizmo(MouseEvent e) throws badSelectException {
        if (currentGizmoToPlace.equalsIgnoreCase("Circle")) {

            if (isSpaceAvailable(e, "Circle", "")) {
                addCircleGizmo(e);
            } else {
                JOptionPane.showMessageDialog(null, "Space needed by another Gizmo", "Invalid Space", JOptionPane.ERROR_MESSAGE);
            }

        } else if (currentGizmoToPlace.equalsIgnoreCase("Square")) {

            if (isSpaceAvailable(e, "Square", "")) {
                addSquareGizmo(e);
            } else {
                JOptionPane.showMessageDialog(null, "Space needed by another Gizmo", "Invalid Space", JOptionPane.ERROR_MESSAGE);
            }

        } else if (currentGizmoToPlace.equalsIgnoreCase("Triangle")) {

            if (isSpaceAvailable(e, "Triangle", "")) {
                addTriangleGizmo(e);
            } else {
                JOptionPane.showMessageDialog(null, "Space needed by another Gizmo", "Invalid Space", JOptionPane.ERROR_MESSAGE);
            }

        } else if (currentGizmoToPlace.equalsIgnoreCase("Absorber")) {

            clearRemoveList();
            addAbsorberGizmo(e);


        } else if (currentGizmoToPlace.equalsIgnoreCase("Left flipper")) {

            if (isSpaceAvailable(e, "LeftFlipper", "")) {
                addLeftFlipperGizmo(e);
            } else {
                JOptionPane.showMessageDialog(null, "Space needed by another Gizmo", "Invalid Space", JOptionPane.ERROR_MESSAGE);
            }

        } else if (currentGizmoToPlace.equalsIgnoreCase("Right flipper")) {

            if (isSpaceAvailable(e, "RightFlipper", "")) {
                addRightFlipperGizmo(e);
            } else {
                JOptionPane.showMessageDialog(null, "Space needed by another Gizmo", "Invalid Space", JOptionPane.ERROR_MESSAGE);
            }

        } else if (currentGizmoToPlace.equalsIgnoreCase("Teleporter")) {
            if (isSpaceAvailable(e, "Teleporter", "")) {
                addTeleporterGizmo(e);
            } else {
                JOptionPane.showMessageDialog(null, "Space needed by another Gizmo", "Invalid Space", JOptionPane.ERROR_MESSAGE);
            }
        }

        model.updateObservers();
    }

    private void addTeleporterGizmo(MouseEvent e) {
        int TPID = model.generateGizmoID("Teleporter");

        try {
            model.addGizmo(new TelePorter("TP" + TPID, e.getX() / PIXELS_PER_GRID_BOX, e.getY() / PIXELS_PER_GRID_BOX), "TP" + TPID);
            infoLabel.setText("Teleporter with ID " + TPID + " added to " + e.getX() / PIXELS_PER_GRID_BOX + "X " + e.getY() / PIXELS_PER_GRID_BOX + "Y");
        } catch (DuplicateGizmoException e1) {
            e1.printStackTrace();
        } catch (badSelectException e2) {
            infoLabel.setText("Invalid Co-ordinates!");
            infoLabel.setBackground(Color.red);
            infoLabel.setBorder(BorderFactory.createBevelBorder(1));
        }
    }

    private void addRightFlipperGizmo(MouseEvent e) {
        int rightFlipperID = model.generateGizmoID("RightFlipper");

        try {
            model.addGizmo(new RightFlipper("RF" + rightFlipperID, e.getX() / PIXELS_PER_GRID_BOX, e.getY() / PIXELS_PER_GRID_BOX), "RF" + rightFlipperID);
            infoLabel.setText("RightFlipper with ID " + rightFlipperID + " added to " + e.getX() / PIXELS_PER_GRID_BOX + "X " + e.getY() / PIXELS_PER_GRID_BOX + "Y");
        } catch (DuplicateGizmoException e1) {
            e1.printStackTrace();
        } catch (badSelectException e2) {
            infoLabel.setText("Invalid Co-ordinates!");
            infoLabel.setBackground(Color.red);
            infoLabel.setBorder(BorderFactory.createBevelBorder(1));
        }
    }

    private void addLeftFlipperGizmo(MouseEvent e) {
        int leftFlipperID = model.generateGizmoID("LeftFlipper");

        try {
            model.addGizmo(new LeftFlipper("LF" + leftFlipperID, e.getX() / PIXELS_PER_GRID_BOX, e.getY() / PIXELS_PER_GRID_BOX), "LF" + leftFlipperID);
            infoLabel.setText("LeftFlipper with ID " + leftFlipperID + " added to " + e.getX() / PIXELS_PER_GRID_BOX + "X " + e.getY() / PIXELS_PER_GRID_BOX + "Y");
        } catch (DuplicateGizmoException e1) {
            e1.printStackTrace();
        } catch (badSelectException e2) {
            infoLabel.setText("Invalid Co-ordinates!");
            infoLabel.setBackground(Color.red);
            infoLabel.setBorder(BorderFactory.createBevelBorder(1));

        }
    }

    private void clearRemoveList() {
        for (String s : removeList) {
            try {
                model.removeGizmo(s);
            } catch (noSuchConnectionException e1) {
                infoLabel.setText("Something went wrong on our end!");
                infoLabel.setBackground(Color.RED);
            }
        }
        removeList.clear();
    }

    private void addAbsorberGizmo(MouseEvent e) {
        int absorberID = model.generateGizmoID("Absorber");
        try {

            //TODO may not need all of these but defo need for x2 and y2 (for now)
            if (absorberX1 < 0) {
                absorberX1 = 0;
            }
            if (absorberX2 > 19) {
                absorberX2 = 19;
            }
            if (absorberY1 < 0) {
                absorberY1 = 0;
            }
            if (absorberY2 > 19) {
                absorberY2 = 19;
            }

            if (absorberX2 >= absorberX1 && absorberY2 >= absorberY1) {
                absorberX2++;
                absorberY2++;
                if (isSpaceAvailable(e, "Absorber", "")) {
                    model.addGizmo(new Absorber("A" + absorberID, absorberX1, absorberY1, absorberX2, absorberY2), "A" + absorberID);
                    infoLabel.setText("Absorber with ID " + absorberID + " added starting at " + e.getX() / PIXELS_PER_GRID_BOX + "X " + e.getY() / PIXELS_PER_GRID_BOX + "Y");
                    infoLabel.setBackground(Color.green);
                    infoLabel.setBorder(BorderFactory.createBevelBorder(1));
                } else {
                    JOptionPane.showMessageDialog(null, "Invalid placement of the absorber!", "Invalid Space", JOptionPane.ERROR_MESSAGE);
                }
            } else if (absorberX2 < absorberX1 && absorberY2 < absorberY1) {
                absorberX1++;
                absorberY1++;
                if (isSpaceAvailable(e, "Absorber", "")) {
                    model.addGizmo(new Absorber("A" + absorberID, absorberX2, absorberY2, absorberX1, absorberY1), "A" + absorberID);
                    infoLabel.setText("Absorber with ID " + absorberID + " added starting at " + e.getX() / PIXELS_PER_GRID_BOX + "X " + e.getY() / PIXELS_PER_GRID_BOX + "Y");
                    infoLabel.setBackground(Color.green);
                    infoLabel.setBorder(BorderFactory.createBevelBorder(1));
                } else {
                    JOptionPane.showMessageDialog(null, "Invalid placement of the absorber!", "Invalid Space", JOptionPane.ERROR_MESSAGE);
                }
            } else if (absorberX2 < absorberX1 && absorberY2 >= absorberY1) {
                absorberX1++;
                absorberY2++;
                if (isSpaceAvailable(e, "Absorber", "")) {
                    model.addGizmo(new Absorber("A" + absorberID, absorberX2, absorberY1, absorberX1, absorberY2), "A" + absorberID);
                    infoLabel.setText("Absorber with ID " + absorberID + " added starting at " + e.getX() / PIXELS_PER_GRID_BOX + "X " + e.getY() / PIXELS_PER_GRID_BOX + "Y");
                    infoLabel.setBackground(Color.green);
                    infoLabel.setBorder(BorderFactory.createBevelBorder(1));
                } else {
                    JOptionPane.showMessageDialog(null, "Invalid placement of the absorber!", "Invalid Space", JOptionPane.ERROR_MESSAGE);
                }
            } else if (absorberX2 >= absorberX1 && absorberY2 < absorberY1) {
                absorberX2++;
                absorberY1++;
                if (isSpaceAvailable(e, "Absorber", "")) {
                    model.addGizmo(new Absorber("A" + absorberID, absorberX1, absorberY2, absorberX2, absorberY1), "A" + absorberID);
                    infoLabel.setText("Absorber with ID " + absorberID + " added starting at " + e.getX() / PIXELS_PER_GRID_BOX + "X " + e.getY() / PIXELS_PER_GRID_BOX + "Y");
                    infoLabel.setBackground(Color.green);
                    infoLabel.setBorder(BorderFactory.createBevelBorder(1));
                } else {
                    JOptionPane.showMessageDialog(null, "Invalid placement of the absorber!", "Invalid Space", JOptionPane.ERROR_MESSAGE);
                }
            }
        } catch (DuplicateGizmoException e1) {
            e1.printStackTrace();
        } catch (badSelectException e2) {
            infoLabel.setText("Bad Coordinates!");
            infoLabel.setBackground(Color.RED);
            infoLabel.setBorder(BorderFactory.createBevelBorder(1));
        }
    }

    private void addTriangleGizmo(MouseEvent e) {
        int triangleID = model.generateGizmoID("Triangle");

        try {
            model.addGizmo(new Triangle("T" + triangleID, e.getX() / PIXELS_PER_GRID_BOX, e.getY() / PIXELS_PER_GRID_BOX), "T" + triangleID);
            infoLabel.setText("Triangle with ID " + triangleID + " added to " + e.getX() / PIXELS_PER_GRID_BOX + "X " + e.getY() / PIXELS_PER_GRID_BOX + "Y");
            infoLabel.setBackground(Color.green);
            infoLabel.setBorder(BorderFactory.createBevelBorder(1));
        } catch (DuplicateGizmoException e1) {
            e1.printStackTrace();
        } catch (badSelectException e2) {
            System.out.println("Invalid Co-ordinates!");
        }
    }

    private void addSquareGizmo(MouseEvent e) {
        int squareID = model.generateGizmoID("Square");

        try {
            model.addGizmo(new Square("S" + squareID, e.getX() / PIXELS_PER_GRID_BOX, e.getY() / PIXELS_PER_GRID_BOX), "S" + squareID);
            infoLabel.setText("Square with ID " + squareID + " added to " + e.getX() / PIXELS_PER_GRID_BOX + "X " + e.getY() / PIXELS_PER_GRID_BOX + "Y");
            infoLabel.setBackground(Color.green);
            infoLabel.setBorder(BorderFactory.createBevelBorder(1));
        } catch (DuplicateGizmoException e1) {
            e1.printStackTrace();
        } catch (badSelectException e2) {
            System.out.println("Invalid Co-ordinates!");
        }
    }

    private void addCircleGizmo(MouseEvent e) {
        int circleID = model.generateGizmoID("Circle");
        try {
            model.addGizmo(new CircleGizmo("C" + circleID, e.getX() / PIXELS_PER_GRID_BOX, e.getY() / PIXELS_PER_GRID_BOX), "C" + circleID);
            infoLabel.setText("Circle with ID " + circleID + " added to " + e.getX() / PIXELS_PER_GRID_BOX + "X " + e.getY() / PIXELS_PER_GRID_BOX + "Y");
            infoLabel.setBackground(Color.green);
            infoLabel.setBorder(BorderFactory.createBevelBorder(1));
        } catch (DuplicateGizmoException e1) {
            e1.printStackTrace();
        } catch (badSelectException e2) {
            System.out.println("Invalid Co-ordinates!");
        }
    }

    private void drawAbsorberOnDrag(MouseEvent e) {

        for (String s : removeList) {
            try {
                model.removeGizmo(s);
            } catch (noSuchConnectionException e1) {
                System.out.println("Shoddy code!");
            }
        }
        if (model.getAddGizmoMode()) {
            if (currentGizmoToPlace.equalsIgnoreCase("Absorber")) {
                int absorberID = model.generateGizmoID("Absorber");
                try {


                    if (e.getX() / PIXELS_PER_GRID_BOX >= absorberX1 && e.getY() / PIXELS_PER_GRID_BOX >= absorberY1) {
                        model.addGizmo(new Absorber("A" + absorberID, absorberX1, absorberY1, e.getX() / PIXELS_PER_GRID_BOX + 1, e.getY() / PIXELS_PER_GRID_BOX + 1), "A" + absorberID);
                    } else if (e.getX() / PIXELS_PER_GRID_BOX < absorberX1 && e.getY() / PIXELS_PER_GRID_BOX < absorberY1) {
                        model.addGizmo(new Absorber("A" + absorberID, e.getX() / PIXELS_PER_GRID_BOX, e.getY() / PIXELS_PER_GRID_BOX, absorberX1 + 1, absorberY1 + 1), "A" + absorberID);
                    } else if (e.getX() / PIXELS_PER_GRID_BOX <= absorberX1 && e.getY() / PIXELS_PER_GRID_BOX >= absorberY1) {
                        model.addGizmo(new Absorber("A" + absorberID, e.getX() / PIXELS_PER_GRID_BOX, absorberY1, absorberX1 + 1, e.getY() / PIXELS_PER_GRID_BOX + 1), "A" + absorberID);
                    } else if (e.getX() / PIXELS_PER_GRID_BOX >= absorberX1 && e.getY() / PIXELS_PER_GRID_BOX <= absorberY1) {
                        model.addGizmo(new Absorber("A" + absorberID, absorberX1, e.getY() / PIXELS_PER_GRID_BOX, e.getX() / PIXELS_PER_GRID_BOX + 1, absorberY1 + 1), "A" + absorberID);
                    }
                    removeList.add("A" + absorberID);
                    model.updateObservers();

                } catch (DuplicateGizmoException e1) {
                    e1.printStackTrace();
                } catch (badSelectException e2) {
                    infoLabel.setText("Invalid placement");
                    infoLabel.setBackground(Color.red);
                    infoLabel.setBorder(BorderFactory.createBevelBorder(1));
                }

            }
        }
    }

    private void addBall(MouseEvent e) {

        if (isSpaceAvailableForBall(e)) {
            JDialog jDialog = new JDialog();
            JPanel topPanel = new JPanel();
            JPanel bottomPanel = new JPanel();
            topPanel.setLayout(new GridLayout(2, 2));
            jDialog.setLayout(new GridLayout(2, 1));
            jDialog.setLocationRelativeTo(null);
            jDialog.setTitle("Select ball velocity");
            JLabel xVeloLabel = new JLabel("Ball's x velocity");
            JLabel yVeloLabel = new JLabel("Ball's y velocity");
            final int velocity_min = -250;
            final int velocity_max = 250;
            final int velocity_default = 0;
            JSlider xVelo = new JSlider(JSlider.HORIZONTAL, velocity_min, velocity_max, velocity_default);
            xVelo.setToolTipText("Set's the x velocity of the new ball");
            JSlider yVelo = new JSlider(JSlider.HORIZONTAL, velocity_min, velocity_max, velocity_default);
            yVelo.setToolTipText("Set's the y velocity of the new ball");
            Hashtable labelTable = new Hashtable();
            labelTable.put(-250, new JLabel("-25"));
            labelTable.put(-125, new JLabel("-12.5"));
            labelTable.put(0, new JLabel("0"));
            labelTable.put(125, new JLabel("12.5"));
            labelTable.put(250, new JLabel("25"));

            xVelo.setLabelTable(labelTable);
            xVelo.setMajorTickSpacing(25);
            xVelo.setPaintTicks(true);
            xVelo.setPaintLabels(true);
            yVelo.setLabelTable(labelTable);
            yVelo.setMajorTickSpacing(25);
            yVelo.setPaintTicks(true);
            yVelo.setPaintLabels(true);
            JButton submitButton = new JButton("Add ball");
            submitButton.addActionListener(new SubmitBallListener(model, e.getX(), e.getY(), PIXELS_PER_GRID_BOX, xVelo, yVelo, jDialog));
            topPanel.add(xVeloLabel);
            topPanel.add(xVelo);
            topPanel.add(yVeloLabel);
            topPanel.add(yVelo);
            bottomPanel.add(submitButton);
            jDialog.add(topPanel);
            jDialog.add(bottomPanel);
            jDialog.pack();
            jDialog.setModal(true);
            jDialog.setVisible(true);
        } else {
            infoLabel.setText("Invalid placement");
            infoLabel.setBackground(Color.red);
            infoLabel.setBorder(BorderFactory.createBevelBorder(1));
            JOptionPane.showMessageDialog(null, "Space needed by another Gizmo", "Invalid Space", JOptionPane.ERROR_MESSAGE);

        }
    }

    private boolean isSpaceAvailableForBall(MouseEvent e) {
        Ball newBall = new Ball("newBall", e.getX() / PIXELS_PER_GRID_BOX, e.getY() / PIXELS_PER_GRID_BOX, 0, 0);
        double radius = (newBall.getRadius() * PIXELS_PER_GRID_BOX);
        for (Gizmo gizmo : model.getGizmos()) {
            if (!gizmo.getType().equalsIgnoreCase("Circle") && !gizmo.getType().equalsIgnoreCase("LeftFlipper") && !gizmo.getType().equalsIgnoreCase("RightFlipper")) {
                for (int i = 0; i < 2 * Math.PI; i++) {
                    try {
                        String gizmoSpot = model.getGizmoAt((int) Math.floor(((e.getX() + radius * Math.cos(i)) / PIXELS_PER_GRID_BOX)), (int) Math.floor((e.getY() + radius * Math.sin(i)) / PIXELS_PER_GRID_BOX));
                        if (gizmo.getID().equals(gizmoSpot) && !gizmo.getType().equalsIgnoreCase("Circle") && !gizmo.getType().equalsIgnoreCase("Ball")) {
                            System.out.println(gizmo.getType());
                            return false;
                        }

                    } catch (badSelectException e1) {
                        e1.printStackTrace();
                    }
                }

            } else if (gizmo.getType().equalsIgnoreCase("Circle")) {
                CircleGizmo circle = (CircleGizmo) gizmo;
                double distance = ((float) e.getX() / PIXELS_PER_GRID_BOX - circle.getCircle().getCenter().x()) * ((float) e.getX() / PIXELS_PER_GRID_BOX - circle.getCircle().getCenter().x()) + ((float) e.getY() / PIXELS_PER_GRID_BOX - circle.getCircle().getCenter().y()) * ((float) e.getY() / PIXELS_PER_GRID_BOX - circle.getCircle().getCenter().y());

                if (distance <= ((circle.getCircle().getRadius() + newBall.getRadius()) * (circle.getCircle().getRadius() + newBall.getRadius()))) {
                    return false;
                }
            } else if (gizmo.getType().equalsIgnoreCase("LeftFlipper") || gizmo.getType().equalsIgnoreCase("RightFlipper")) {
                for (int i = 0; i < 2 * Math.PI; i++) {
                    if (gizmo.getX() <= (e.getX() + radius * Math.cos(i)) / PIXELS_PER_GRID_BOX && (gizmo.getX() + 1) >= (e.getX() + radius * Math.cos(i)) / PIXELS_PER_GRID_BOX && gizmo.getY() <= (e.getY() + radius * Math.sin(i)) / PIXELS_PER_GRID_BOX && (gizmo.getY() + 2) >= (e.getY() + radius * Math.sin(i)) / PIXELS_PER_GRID_BOX) {
                        return false;
                    }
                }
            }
        }

        for (Ball ball : model.getBalls()) {
            double distance = ((float) e.getX() / PIXELS_PER_GRID_BOX - (float) ball.getCircle().getCenter().x()) * ((float) e.getX() / PIXELS_PER_GRID_BOX - (float) ball.getCircle().getCenter().x()) + ((float) e.getY() / PIXELS_PER_GRID_BOX - (float) ball.getCircle().getCenter().y()) * ((float) e.getY() / PIXELS_PER_GRID_BOX - (float) ball.getCircle().getCenter().y());
            if (distance <= ((ball.getCircle().getRadius() + newBall.getRadius()) * (ball.getCircle().getRadius() + newBall.getRadius()))) {
                return false;
            }
        }

        return !((float) (e.getX() - radius) / PIXELS_PER_GRID_BOX <= 0) && !((float) (e.getX() + radius) / PIXELS_PER_GRID_BOX >= 20) && !((float) (e.getY() - radius) / PIXELS_PER_GRID_BOX <= 0) && !((float) (e.getY() + radius) / PIXELS_PER_GRID_BOX >= 20);
    }

    private void disconnect(MouseEvent e) {

        if (clickcount == 0) {
            try {
                String ID1 = model.getGizmoAt(e.getX() / 25.0, e.getY() / 25.0);
                if (!ID1.isEmpty() && !(ID1.charAt(0) == 'B')) {
                    infoLabel.setText("First Gizmo clicked: " + model.getGizmoAt(e.getX() / 25.0, e.getY() / 25.0));
                    clickcount = 1;
                } else {
                    infoLabel.setText("You didn't click a gizmo!");
                    infoLabel.setBackground(Color.RED);
                    infoLabel.setBorder(BorderFactory.createBevelBorder(1));
                }
            } catch (badSelectException error) {
                JOptionPane.showConfirmDialog(null, "Bad selection", "No gizmo there!", JOptionPane.DEFAULT_OPTION);
                return;
            }
            twopoint[0] = e.getPoint();


        } else if (clickcount == 1) {
            double x, y;
            Point p = e.getPoint();
            x = (int) p.getX() / PIXELS_PER_GRID_BOX;
            y = (int) p.getY() / PIXELS_PER_GRID_BOX;
            try {
                model.removeConnection(model.getGizmoAt((int) twopoint[0].getX() / PIXELS_PER_GRID_BOX, (int) twopoint[0].getY() / PIXELS_PER_GRID_BOX), model.getGizmoAt((int) x, (int) y));
                infoLabel.setText("Disconnected " + model.getGizmoAt((int) twopoint[0].getX() / PIXELS_PER_GRID_BOX, (int) twopoint[0].getY() / PIXELS_PER_GRID_BOX) + " from " + model.getGizmoAt((int) x, (int) y));
            } catch (badSelectException e1) {
                JOptionPane.showConfirmDialog(null, "Bad selection", "No gizmo there!", JOptionPane.DEFAULT_OPTION);
            } catch (noSuchConnectionException e2) {
                JOptionPane.showConfirmDialog(null, "Bad selection", "No connection from one gizmo to the second!", JOptionPane.DEFAULT_OPTION);
            }
            clickcount = 0;
            twopoint[0] = null;
            model.setSelectedMode(true);


        }
    }

    private void connect(MouseEvent e) {


        if (clickcount == 0) {
            try {
                if (!model.getGizmoAt(e.getX() / 25.0, e.getY() / 25.0).isEmpty() && !(model.getGizmoAt(e.getX() / 25.0, e.getY() / 25.0).charAt(0) == 'B')) {
                    infoLabel.setText("First Gizmo clicked: " + model.getGizmoAt(e.getX() / 25.0, e.getY() / 25.0));
                } else {
                    infoLabel.setText("Ball or nothing clicked!");
                    infoLabel.setBackground(Color.red);
                    infoLabel.setBorder(BorderFactory.createBevelBorder(1));
                }
            } catch (badSelectException error) {
                JOptionPane.showConfirmDialog(null, "Bad selection", "No gizmo there!", JOptionPane.DEFAULT_OPTION);
                return;
            }
            twopoint[0] = e.getPoint();
            clickcount = 1;
            System.out.println("Got first gizmo, update JLabel");

        } else if (clickcount == 1) {
            double x, y;
            Point p = e.getPoint();
            x = (int) p.getX() / PIXELS_PER_GRID_BOX;
            y = (int) p.getY() / PIXELS_PER_GRID_BOX;
            try {
                model.addConnection(model.getGizmoAt((int) twopoint[0].getX() / PIXELS_PER_GRID_BOX, (int) twopoint[0].getY() / PIXELS_PER_GRID_BOX), model.getGizmoAt((int) x, (int) y));
                infoLabel.setText("Connection from: " + model.getGizmoAt((int) twopoint[0].getX() / PIXELS_PER_GRID_BOX, (int) twopoint[0].getY() / PIXELS_PER_GRID_BOX) + " to " + model.getGizmoAt((int) x, (int) y));
                infoLabel.setBorder(BorderFactory.createBevelBorder(1));
                infoLabel.setBackground(Color.green);
            } catch (badSelectException e1) {
                infoLabel.setText("No gizmo clicked!");
                infoLabel.setBorder(BorderFactory.createBevelBorder(1));
                infoLabel.setBackground(Color.red);
            } catch (noSuchConnectionException e1) {
                infoLabel.setText(e1.getMessage());
                infoLabel.setBorder(BorderFactory.createBevelBorder(1));
                infoLabel.setBackground(Color.red);
            }
            clickcount = 0;
            twopoint[0] = null;
            model.setSelectedMode(true);

        }
    }

    private void move(MouseEvent e) {

        if (clickcount == 0) {

            try {
                String ID1 = model.getGizmoAt(e.getX() / 25.0, e.getY() / 25.0);
                if (!ID1.isEmpty()) {
                    infoLabel.setText("First Gizmo clicked: " + model.getGizmoAt(e.getX() / 25.0, e.getY() / 25.0));
                    infoLabel.setBackground(Color.green);
                    infoLabel.setBorder(BorderFactory.createBevelBorder(1));
                } else {
                    infoLabel.setText("Bad click!");
                    infoLabel.setBackground(Color.red);
                    infoLabel.setBorder(BorderFactory.createBevelBorder(1));
                }
            } catch (badSelectException error) {
                JOptionPane.showConfirmDialog(null, "Bad selection", "No gizmo there!", JOptionPane.DEFAULT_OPTION);
                return;
            }
            twopoint[0] = e.getPoint();

            clickcount = 1;

        } else if (clickcount == 1) {
            double x, y;
            Point p = e.getPoint();
            x = p.getX() / PIXELS_PER_GRID_BOX;
            y = p.getY() / PIXELS_PER_GRID_BOX;
            try {
                String objectToMove = model.getGizmoAt(twopoint[0].getX() / PIXELS_PER_GRID_BOX, twopoint[0].getY() / PIXELS_PER_GRID_BOX);
                boolean isBall = false;
                boolean objectMovable = false;

                for (Ball ball : model.getBalls()) {
                    if (ball.getID().equalsIgnoreCase(objectToMove)) {
                        isBall = true;
                    }
                }

                if (isBall) {
                    if (isSpaceAvailableForBall(e)) {
                        objectMovable = true;
                    }
                } else {
                    String gizmoType = "Square";
                    for (Gizmo gizmo : model.getGizmos()) {
                        if (gizmo.getID().equalsIgnoreCase(objectToMove)) {
                            gizmoType = gizmo.getType();
                            if (gizmoType.equalsIgnoreCase("Absorber")) {
                                Absorber a = (Absorber) gizmo;
                                absorberX1 = a.getX();
                                absorberX2 = a.getX2Coord();
                                absorberY1 = a.getY();
                                absorberY2 = a.getY2Coord();
                            }
                        }
                    }
                    if (isSpaceAvailable(e, gizmoType, objectToMove)) {
                        objectMovable = true;
                    }
                }
                if (!objectToMove.isEmpty()) {
                    if (objectMovable) {
                        if (twopoint[0] != null) {
                            String ID1 = model.getGizmoAt(twopoint[0].getX() / PIXELS_PER_GRID_BOX, twopoint[0].getY() / PIXELS_PER_GRID_BOX);
                            if (!ID1.isEmpty()) {
                                model.moveGizmo(ID1, x, y);
                                infoLabel.setText("Moving: " + model.getGizmoAt(e.getX() / PIXELS_PER_GRID_BOX, e.getY() / PIXELS_PER_GRID_BOX) + " to " + (int) x + "X " + (int) y + "Y");
                                infoLabel.setBackground(Color.green);
                                infoLabel.setBorder(BorderFactory.createBevelBorder(1));
                            }

                        }
                    } else {
                        infoLabel.setText("Bad click, space needed!");
                        infoLabel.setBackground(Color.red);
                        infoLabel.setBorder(BorderFactory.createBevelBorder(1));
                        JOptionPane.showMessageDialog(null, "Invalid placement", "Invalid Space", JOptionPane.ERROR_MESSAGE);
                    }
                } else {
                    infoLabel.setText("No gizmo clicked!");
                    infoLabel.setBackground(Color.red);
                    infoLabel.setBorder(BorderFactory.createBevelBorder(1));
                }

            } catch (badSelectException e1) {
                e1.printStackTrace();
            } catch (NullPointerException e2) {
                infoLabel.setText("You didn't click a gizmo!");
                infoLabel.setBackground(Color.RED);

            }

            clickcount = 0;
            twopoint[0] = null;

        }

    }

    private void delete(MouseEvent e) {
        double x, y;
        Point p = e.getPoint();
        x = p.getX() / PIXELS_PER_GRID_BOX;
        y = p.getY() / PIXELS_PER_GRID_BOX;
        if (model.getDeleteMode()) {
            String ID;
            try {
                ID = model.getGizmoAt(x, y);
                if (!ID.isEmpty()) {
                    model.removeGizmo(ID);
                    infoLabel.setText("Gizmo with ID " + ID + " deleted");
                    infoLabel.setBackground(Color.green);
                    infoLabel.setBorder(BorderFactory.createBevelBorder(1));
                } else {
                    infoLabel.setText("You didn't click a gizmo!");
                    infoLabel.setBackground(Color.RED);
                    infoLabel.setBorder(BorderFactory.createBevelBorder(1));
                }
            } catch (badSelectException e1) {
                JOptionPane.showConfirmDialog(null, "Bad selection", "No gizmo there!", JOptionPane.DEFAULT_OPTION);
            } catch (noSuchConnectionException e1) {
                System.out.println("Shoddy code!");
            }
        }

    }

    private void rotate(MouseEvent e) {
        double x, y;
        Point p = e.getPoint();
        x = (int) p.getX() / PIXELS_PER_GRID_BOX;
        y = (int) p.getY() / PIXELS_PER_GRID_BOX;
        if (model.getRotateMode()) {
            try {
                String ID = model.getGizmoAt(x, y);
                if (!ID.isEmpty() && !(ID.charAt(0) == 'A')) {
                    model.rotateGizmo(model.getGizmoAt((int) x, (int) y));
                    infoLabel.setText("Gizmo with ID " + ID + " rotated");
                    infoLabel.setBackground(Color.green);
                    infoLabel.setBorder(BorderFactory.createBevelBorder(1));
                } else if (!ID.isEmpty() && ID.charAt(0) == 'A') {
                    infoLabel.setText("You cannot rotate an absorber!");
                    infoLabel.setBackground(Color.RED);
                    infoLabel.setBorder(BorderFactory.createBevelBorder(1));
                } else {
                    infoLabel.setText("You didn't click a gizmo!");
                    infoLabel.setBackground(Color.RED);
                    infoLabel.setBorder(BorderFactory.createBevelBorder(1));
                }
            } catch (badSelectException e1) {
                JOptionPane.showConfirmDialog(null, "Bad selection", "No gizmo there!", JOptionPane.DEFAULT_OPTION);
            }
        }

    }

    @Override
    public void mousePressed(MouseEvent e) {
        absorberX1 = e.getX() / PIXELS_PER_GRID_BOX;
        absorberY1 = e.getY() / PIXELS_PER_GRID_BOX;
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        absorberX2 = e.getX() / PIXELS_PER_GRID_BOX;
        absorberY2 = e.getY() / PIXELS_PER_GRID_BOX;


        if (model.getAddGizmoMode()) {
            if (currentGizmoToPlace.equalsIgnoreCase("Absorber")) {
                try {
                    addGizmo(e);
                } catch (badSelectException e1) {
                    e1.printStackTrace();
                }
            }
        }
    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    @Override
    public void mouseDragged(MouseEvent e) {
        drawAbsorberOnDrag(e);
    }

    @Override
    public void mouseMoved(MouseEvent e) {

    }
}
