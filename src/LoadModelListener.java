import Exceptions.badFileException;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.text.ParseException;

public class LoadModelListener implements ActionListener {
    private fakeModel model;
    private GizmoView view;
    public LoadModelListener(fakeModel model, GizmoView view) {
        this.model = model;
        this.view = view;
    }
    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        final JFileChooser fileChooser = new JFileChooser();
        int returnVal = fileChooser.showOpenDialog(null);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            File file = fileChooser.getSelectedFile();
            model.setFile(file);
            GizmoLoader gizmoLoader = new GizmoLoader(file);
            try {
                model.clear();
                gizmoLoader.readFile(model);
                view.updateSliders();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ParseException e) {
                JOptionPane.showMessageDialog(null, "File is in an invalid format");
            } catch (badFileException e) {
                if (!e.getMessage().isEmpty()) {
                    JOptionPane.showMessageDialog(null, e.getMessage());

                } else {
                    JOptionPane.showMessageDialog(null, "File has errors in it");
                }
            }
        }
    }


}
