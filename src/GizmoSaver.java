import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class GizmoSaver {

    private BufferedWriter fileInput;


    public GizmoSaver(String filename) {
        try{
            File f = new File(filename);
            fileInput = new BufferedWriter(new FileWriter(f));
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void saveFile(fakeModel Model) throws IOException {
        for (Gizmo g : Model.getGizmos()){
          if(g instanceof  Absorber){
            Absorber abs = (Absorber) g;
              fileInput.write(abs.getType() + " " + abs.getID() + " " + abs.getX() + " " + abs.getY() + " " + abs.getX2Coord() + " " + abs.getY2Coord());
            fileInput.write("\n");
            continue;
          }
            fileInput.write(g.getType() + " " + g.getID() + " " + g.getX() + " " + g.getY());
          StringBuilder s = new StringBuilder();
          switch (g.getRotation().getNumVal()){
            case 1:
              s.append("\nRotate ").append(g.getID());
              break;
            case 2:
              break;
            case 3:
              s.append("\nRotate ").append(g.getID());
              s.append("\nRotate ").append(g.getID());
            case 4:
              s.append("\nRotate ").append(g.getID());
          }

            fileInput.write("\n");
          fileInput.write(s.append("\n").toString());
        }
        for(Gizmo g : Model.getGizmos()){
            for(String s : g.getGizmoConnections()){
                fileInput.write("Connect "+ g.getID() + " " + s);
                fileInput.write("\n");
            }
        }
        for(Gizmo g : Model.getGizmos()){
          for(String keyConnectString : g.getKeyConnections()){
            StringBuilder s = new StringBuilder();
            String[] strings = keyConnectString.split("\\.",104);
            s.append("\nKeyConnect");
            s.append(" key ").append(strings[1]);
            s.append(" ").append(strings[2]);
            s.append(" ").append(g.getID());
            s.append("\n");
            fileInput.write(s.toString());
          }
        }


        for(Ball b : Model.getBalls()){
            fileInput.write(b.toString());
            fileInput.write("\n");
        }

        fileInput.write("\n");
        fileInput.write(Model.getWalls().toString());
        if(Double.parseDouble(Model.getFrictionOverDistance()) != 0.025){
          fileInput.write("Friction " + Model.getFrictionOverDistance() + " ");
        }
      if(Double.parseDouble(Model.getFrictionOverTime()) != 0.025){
        fileInput.write(Model.getFrictionOverTime());
        fileInput.write("\n");
      }
      if(Double.parseDouble(Model.getGravity()) != 25.0) {
        fileInput.write("Gravity " + Model.getGravity());
        fileInput.write("\n");
      }
      fileInput.close();

    }



}
