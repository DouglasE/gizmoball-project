import physics.Circle;
import physics.Vect;

import java.awt.*;

public class Ball {
  private final String ID;
  private double xCoord;
  private double yCoord;
  private Vect velocity;
  private boolean stopped;
  private  double radius;
  private boolean isActive;
  private Vect origin;
  private Color color;
  private Vect originVect;
  private int  collisionCount;
  private Gizmo lastHit;
  public Ball(String ID, double xCoord, double yCoord, double xVelo, double yVelo) {
    this.ID = ID;
    this.xCoord = xCoord;
    this.yCoord = yCoord;
    this.color = Color.yellow;
    this.velocity = new Vect(xVelo, yVelo);
    this.stopped = true;
    this.radius = 0.20D;
    this.isActive = true;
    origin = new Vect(xCoord, yCoord);
    originVect = new Vect(xVelo,yVelo);
    collisionCount = 0;
    //First one is x coord of the centre, second is ycoord of centre, radius of 10 whats????
  }

  public void incrementCollisioncount(){
    collisionCount++;

  }
  public int getCollisionCount(){
    return collisionCount;
  }
  public void resetCollisionCount(){
    collisionCount = 0;
  }
  public double getX() {
    return xCoord;
  }
  public String getID(){
    return ID;
  }
  public double getY() {
    return yCoord;
  }

  public void setX(double x) {
    this.xCoord = x;
  }

  public void setY(double y) {
    this.yCoord = y;
  }

  public Vect getVelocity() {
    return velocity;
  }
  public void start() {
    this.stopped = false;
  }

  public void stop() {
    stopped = !stopped;
  }

  public boolean isStopped() {
    return this.stopped;
  }

  @Override
  public String toString() {
    return ("Ball " + ID + " " + xCoord + " " + yCoord + " " + velocity.x() + " " + velocity.y());
  }

  public Circle getCircle() {
    return new Circle(xCoord, yCoord, radius);
  }

  public void reset(){
    this.setActive(true);
    this.setX(origin.x());
    this.setY(origin.y());
    this.setVector(originVect);
  }

  public void setVector(Vect v) {
    this.velocity = v;
  }
  public Vect getBounds(){
    return  new Vect(xCoord+0.5,yCoord+0.5);
  }

 @Override
  public int hashCode() {
    return ID.hashCode();
  }

  public double getRadius() {
    return radius;
  }

  public void setActive(boolean active) {
    isActive = active;
  }
  public void setRadius(double radius){
    this.radius = radius;
  }
  public boolean isActive() {
    return isActive;
  }

  @Override
  public boolean equals(Object o){
    if(!(o instanceof  Ball)){
      return false;
    }
    return ((Ball) o).getX() == this.getX() && ((Ball) o).getY() == this.getY()
            && ((Ball) o).getVelocity().equals(this.getVelocity());
  }
  public void setColor(Color c){
    this.color = c;
  }
  public Color getColor(){
    return color;
  }
  public void setLastHit(Gizmo lastHit) {
    this.lastHit = lastHit;
  }

  public Gizmo getLastHit() {
    return lastHit;
  }
}