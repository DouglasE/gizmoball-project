import Exceptions.noSuchConnectionException;
import physics.Circle;
import physics.LineSegment;


import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Triangle implements  Gizmo{
    private final String ID;
    private ROTATION rotation;
    private int XCoord;
    private int YCoord;
    private final int L = 25;
    private List<String> keyConnections;


    private LineSegment side1;
    private LineSegment side2;
    private LineSegment hypotenuse;
    private List<LineSegment> lines;
    private List<Circle> circles;
    private Color color;
    private Color defaultColor;
    private Color triggerColor;
    private Circle ONE;
    private Circle TWO;
    private Circle THREE;
    private List<String> gizmoConnections;


    public Triangle(String ID,int X, int Y)
    {
        defaultColor = Color.blue;
        color = defaultColor;
        triggerColor = Color.cyan;
        gizmoConnections = new ArrayList<>();
        keyConnections = new ArrayList<>();
        this.ID = ID;
        rotation = ROTATION.TOPLEFT;
        this.XCoord  = X;
        this.YCoord = Y;
        lines = new ArrayList<>();

        side1 = new LineSegment(X, Y, (X)+1,Y);
        side2 = new LineSegment(X, Y, X,(Y)+1);
        hypotenuse = new LineSegment(X, (Y)+1, (X) +1,Y);

        lines.add(side1);
        lines.add(side2);
        lines.add(hypotenuse);


        circles = new ArrayList<>();
        int x = this.getX();
        int y = this.getY();
        ONE  = new Circle(x,y,0);
        TWO  = new Circle(x,y+1,0);
        THREE  = new Circle(x+1,y,0);

        circles.add(ONE);
        circles.add(TWO);
        circles.add(THREE);

    }

    public String getID() {
        return   ID;
    }

    public List<LineSegment> getLineSegments() {
        return lines;
    }

    public List<Circle> getCircles() {
        return circles;
    }

    @Override
    public void move(int X, int Y) {

        this.XCoord = X;
        this.YCoord = Y;

        side1 = new LineSegment(X, Y, (X)+1,Y);
        side2 = new LineSegment(X, Y, X,(Y)+1);
        hypotenuse = new LineSegment(X, (Y)+1, (X) +1,Y);

        ONE  = new Circle(X,Y,0);
        TWO  = new Circle(X,Y+1,0);
        THREE  = new Circle(X+1,Y,0);

        lines = new ArrayList<>();
        circles = new ArrayList<>();

        circles.add(ONE);
        circles.add(TWO);
        circles.add(THREE);

        lines.add(side1);
        lines.add(side2);
        lines.add(hypotenuse);
        rotate();
        rotate();
        rotate();
        rotate();

    }

    @Override
    public void rotate() {
        int x = this.getX();
        int y = this.getY();

        switch (rotation){
            case TOPRIGHT:
                lines.remove(side1);
                lines.remove(side2);
                lines.remove(hypotenuse);
                circles.clear();

                side1 = new LineSegment(XCoord, YCoord, (XCoord) +1,YCoord);
                side2 = new LineSegment(XCoord, YCoord, XCoord,(YCoord)+1);
                hypotenuse = new LineSegment(XCoord, (YCoord)+1, (XCoord) +1,YCoord);

                lines.add(side1);
                lines.add(side2);
                lines.add(hypotenuse);

                ONE  = new Circle(x,y,0);
                TWO  = new Circle((x)+1,y,0);
                THREE  = new Circle(x,(y)+1,0);

                circles.add(ONE);
                circles.add(TWO);
                circles.add(THREE);

                rotation = ROTATION.TOPLEFT;
                break;
            case TOPLEFT:

                lines.remove(side1);
                lines.remove(side2);
                lines.remove(hypotenuse);
                circles.clear();

                side1 = new LineSegment(XCoord, YCoord, (XCoord) +1,YCoord);
                side2 = new LineSegment(XCoord, YCoord, (XCoord)+1,(YCoord)+1);
                hypotenuse = new LineSegment((XCoord)+1, YCoord, (XCoord) +1,(YCoord)+1);

                lines.add(side1);
                lines.add(side2);
                lines.add(hypotenuse);

                ONE  = new Circle(x,y,0);
                TWO  = new Circle((x)+1,y,0);
                THREE  = new Circle((x)+1,(y)+1,0);

                circles.add(ONE);
                circles.add(TWO);
                circles.add(THREE);

                rotation =ROTATION.BOTTOMRIGHT;
                break;
            case BOTTOMRIGHT:

                lines.remove(side1);
                lines.remove(side2);
                lines.remove(hypotenuse);
                circles.clear();

                side1 = new LineSegment((XCoord)+1, YCoord, (XCoord) +1,(YCoord)+1);
                side2 = new LineSegment(XCoord, (YCoord)+1, (XCoord)+1,(YCoord)+1);
                hypotenuse = new LineSegment(XCoord, (YCoord)+1, (XCoord) +1,YCoord);

                lines.add(side1);
                lines.add(side2);
                lines.add(hypotenuse);


                ONE  = new Circle((x)+1,y,0);
                TWO  = new Circle((x)+1,(y)+1,0);
                THREE  = new Circle(x,(y)+1,0);

                circles.add(ONE);
                circles.add(TWO);
                circles.add(THREE);


                rotation = ROTATION.BOTTOMLEFT;
                break;
            case BOTTOMLEFT:


                lines.remove(side1);
                lines.remove(side2);
                lines.remove(hypotenuse);
                circles.clear();

                side1 = new LineSegment(XCoord, YCoord, XCoord,(YCoord)+1);
                side2 = new LineSegment(XCoord, (YCoord)+1, (XCoord)+1,(YCoord)+1);
                hypotenuse = new LineSegment(XCoord, YCoord, (XCoord) +1,(YCoord)+1);

                lines.add(side1);
                lines.add(side2);
                lines.add(hypotenuse);


                ONE  = new Circle(x,y,0);
                TWO  = new Circle(x,(y)+1,0);
                THREE  = new Circle((x)+1,(y)+1,0);

                circles.add(ONE);
                circles.add(TWO);
                circles.add(THREE);


                rotation = ROTATION.TOPRIGHT;
                break;
            default:
                break;
        }


    }

    @Override
    public int getX() {
        return XCoord;
    }

    @Override
    public int getY() {
        return YCoord;
    }

    @Override
    public String getType() {
        return "Triangle";
    }

    @Override
    public ROTATION getRotation() {
        return rotation;
    }

    @Override
    public boolean addGizmoConnection(String gizmoToConnect) {
        if(gizmoConnections.contains(gizmoToConnect)){
            return false;
        }
        else{
            return gizmoConnections.add(gizmoToConnect);
        }
    }

    @Override
    public List<String> getGizmoConnections() {
        return gizmoConnections;
    }
    @Override
    public int[] getBoardX() {
        int Xcoord = this.getX();
        switch (rotation.getNumVal()) {
            case 1:
                return new int[]{
                    Xcoord,
                    Xcoord,
                    Xcoord +1};
            case 2:
                return new int[]{
                    Xcoord,
                    Xcoord,
                    Xcoord +1};
            case 3:
                return new int[]{
                    Xcoord +1,
                    Xcoord,
                    Xcoord +1};

            case 4:
                return new int[]{
                    Xcoord,
                    Xcoord +1,
                    Xcoord +1};

        }
        return new int[]{};
    }

    @Override
    public int[] getBoardY() {
        int Ycoord = this.getY();

        switch (rotation.getNumVal()) {
            case 1:
                return new int[]{
                    Ycoord,
                    Ycoord +1,
                    Ycoord +1};
            case 2:
                return new int[]{
                    Ycoord,
                    Ycoord +1,
                    Ycoord};
            case 3:
                return new int[]{
                    Ycoord,
                    Ycoord +1,
                    Ycoord +1};

            case 4:
                return new int[]{
                    Ycoord,
                    Ycoord,
                    Ycoord +1};


        }
        return new int[]{};
    }


    @Override
    public boolean addKeyConnect(String key,String UpOrDown) {
        String keyString  = "key" + "." + key + "." + UpOrDown;
        if(!keyConnections.contains(keyString)) {
            keyConnections.add(keyString);
            return true;
        }else{
            return  false;
        }

    }
    @Override
    public Color getColor() {
        return color;
    }
    @Override
    public String toString(){
        StringBuilder s = new StringBuilder("Triangle " + ID + " " + XCoord + " " + YCoord);
        switch (this.getRotation().getNumVal()){
            case 1:
                s.append("\nRotate ").append(ID);
                break;
            case 2:
                break;
            case 3:
                s.append("\nRotate ").append(ID);
                s.append("\nRotate ").append(ID);
            case 4:
                s.append("\nRotate ").append(ID);
        }

        for(String keyConnectString : keyConnections){
            String[] strings = keyConnectString.split("\\.",104);

            s.append("\nKeyConnect");
            s.append(" key ").append(strings[1]);
            s.append(" ").append(strings[2]);
            s.append(" ").append(this.getID());
        }
        for(String consumer : gizmoConnections){
            s.append("\nConnect").append(" ").append(this.getID()).append(" ").append(consumer);
        }
        return  s.toString();
    }
    @Override
    public List<String> getKeyConnections() {
        return new ArrayList<>(keyConnections);
    }

    @Override
    public void trigger() {
        if(this.color == Color.blue ){
            this.color = Color.cyan;
        }
        else{
            color = Color.blue;
        }

    }



    @Override
    public int hashCode(){
        return ID.hashCode();
    }
    @Override
    public void removeConnection(String remove) throws noSuchConnectionException {
        if(!gizmoConnections.contains(remove)){
            throw new noSuchConnectionException("Connection does not exist!");
        }
        gizmoConnections.remove(remove);

    }

    @Override
    public void removeKeyConnects() {
        keyConnections.clear();
    }

    @Override
    public void reset() {

        this.defaultColor = Color.blue;
        this.color = Color.blue;
    }


    @Override
    public boolean equals(Object o){
        if(o == null){
            return false;
        }
         if(!(o instanceof Triangle)){
            return false;
        }


        return this.getID().equals(((Triangle) o).getID()) && this.getX() == ((Triangle) o).getX() && this.getY() == ((Triangle) o).getY();
    }
    @Override
    public void setColor(Color color) {
        this.color = color;
        this.defaultColor =color;
    }


}
