import Exceptions.noSuchConnectionException;
import physics.Circle;
import physics.LineSegment;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class CircleGizmo implements Gizmo {
  final String ID;

  private  int XCoord;
  private  int YCoord;
  private ROTATION rotation;
  private Circle circle;
  private List<String> keyConnections;
  private List<String> gizmoConnections;
  private Color color;
  private Color defaultColor;
  private Color triggerColor;

  public CircleGizmo(String ID, int X, int Y) {
    rotation = ROTATION.NONE;
    gizmoConnections = new ArrayList<>();
    color = Color.green;
    triggerColor = Color.yellow;
    defaultColor= Color.green;
    keyConnections = new ArrayList<>();
    this.ID = ID;
    this.XCoord = X;
    this.YCoord = Y;
    this.circle = new Circle(X + 0.5, Y + 0.5, 0.5);

  }

  @Override
  public Color getColor() {
    return color;
  }

  @Override
  public String getID() {
    return ID;

  }

  @Override
  public void rotate() {

  }

  @Override
  public int getX() {
    return XCoord;
  }


  @Override
  public int getY() {
    return YCoord;
  }

  @Override
  public String getType() {
    return "Circle";
  }

  @Override
  public ROTATION getRotation() {
    return rotation;
  }

    @Override
    public boolean addGizmoConnection(String gizmoToConnect) {

      if(gizmoConnections.contains(gizmoToConnect)){
          return false;
      }
      else{
          return gizmoConnections.add(gizmoToConnect);
      }
    }

    @Override
    public List<String> getGizmoConnections() {
        return gizmoConnections;
    }

    @Override
  public List<LineSegment> getLineSegments() {
    return new ArrayList<>();
  }

  @Override
  public List<Circle> getCircles() {
    List<Circle> circles = new ArrayList<>();
    circles.add(circle);
    return circles;
  }

  @Override
  public int[] getBoardX() {
    return new int[0];
  }

  @Override
  public int[] getBoardY() {
    return new int[0];
  }

  @Override
  public boolean addKeyConnect(String key, String UpOrDown) {
    String keyString  = "key" + "." + key + "." + UpOrDown;
    if(!keyConnections.contains(keyString)) {
      keyConnections.add(keyString);
      return true;
    }else{
      return  false;
    }

  }

  @Override
  public List<String> getKeyConnections() {
    return new ArrayList<>(keyConnections);
  }

  @Override
  public void trigger() {
    if(this.getColor() == defaultColor){
      this.color = triggerColor;
    }else{
      this.color = defaultColor;
    }

  }

  @Override
  public void move(int x, int y) {
    this.XCoord = x;
    this.YCoord = y;
  }

  @Override
  public void removeConnection(String remove) throws noSuchConnectionException {
    if(!gizmoConnections.contains(remove)){
      throw new noSuchConnectionException("Bad boy!");
    }
    gizmoConnections.remove(remove);
  }

  @Override
  public void removeKeyConnects() {
    keyConnections.clear();
  }

  @Override
  public void reset() {
    this.defaultColor = Color.green;
    this.color = Color.green;
  }

  public Circle getCircle() {
    return circle;
  }

  public String toString() {
    StringBuilder s = new StringBuilder("Circle  " + ID + " " + XCoord + " " + YCoord);
    for (String keyConnectString : keyConnections) {
      String[] strings = keyConnectString.split("\\.", 104);

      s.append("\nKeyConnect");
      s.append(" key ").append(strings[1]);
      s.append(" ").append(strings[2]);
      s.append(" ").append(this.getID());

    }
    for(String consumer : gizmoConnections){
      s.append("\nConnect").append(" ").append(this.getID()).append(" ").append(consumer);
    }
    return s.toString();
  }

  @Override
  public int hashCode() {
    return ID.hashCode();
  }


  @Override
  public boolean equals(Object o){
    if(o == null){
      return false;
    }
    if(!(o instanceof CircleGizmo)){
      return false;
    }
    return this.getID().equals(((CircleGizmo) o).getID()) && this.getX() == ((CircleGizmo) o).getX() && this.getY() == ((CircleGizmo) o).getY();
  }
  @Override
  public void setColor(Color color) {
    this.defaultColor =color;
  }
}
