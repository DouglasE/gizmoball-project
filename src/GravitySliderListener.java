import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class GravitySliderListener implements ChangeListener {
    private fakeModel model;
    public GravitySliderListener(fakeModel model){
        this.model = model;
    }
    @Override
    public void stateChanged(ChangeEvent changeEvent) {
        JSlider source = (JSlider)changeEvent.getSource();
        double newGravity = (double)source.getValue();
        model.setGravity(newGravity);
    }
}