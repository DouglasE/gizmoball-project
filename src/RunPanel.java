import javax.swing.*;
import java.awt.*;

public class RunPanel extends JPanel {
    private Timer timer;
    public RunPanel(fakeModel model, GizmoView view, JLabel infolabel, Timer t) {
        //Set the background colour
        this.timer = t;
        setBackground(Color.LIGHT_GRAY);

        //Create buttons and add listeners
        //Main Control group
        JLabel mainControlLabel = new JLabel("Main Controls");
        mainControlLabel.setHorizontalAlignment(SwingConstants.CENTER);
        mainControlLabel.setVerticalAlignment(SwingConstants.CENTER);
        JButton startButton = new JButton("Start");
        startButton.setToolTipText("Starts the game.");
        startButton.addActionListener(new StartListener(model , startButton, view.getBoard(), timer));
        JButton tickButton = new JButton("Tick");
        tickButton.setToolTipText("Progresses the game by one tick of the game clock, then pauses.");
        tickButton.addActionListener(new TickListener(model, startButton,timer));

        //Model Operations group
        JLabel modelOpsLabel = new JLabel("Model Operations");
        modelOpsLabel.setHorizontalAlignment(SwingConstants.CENTER);
        modelOpsLabel.setVerticalAlignment(SwingConstants.CENTER);
        JButton loadModelButton = new JButton("Load Model");
        loadModelButton.setToolTipText("Select a file from the file system to load a board setup from.");
        loadModelButton.addActionListener(new LoadModelListener(model, view));
        JButton reloadModelButton = new JButton("Reset Model");
        reloadModelButton.setToolTipText("Reloads the board to it's original setup.");
        reloadModelButton.addActionListener(new miscListener(model,infolabel));

        //File Operations group
        JLabel fileOpsLabel = new JLabel("File Operations");
        fileOpsLabel.setHorizontalAlignment(SwingConstants.CENTER);
        fileOpsLabel.setVerticalAlignment(SwingConstants.CENTER);
        JButton buildButton = new JButton("Build mode");
        buildButton.setToolTipText("Switches to build mode, so that you can edit the board.");
        buildButton.addActionListener(new BuildModeSwitchListener(model, startButton,infolabel));
        JButton quitButton = new JButton("Quit");
        quitButton.setToolTipText("Quits the game, after a prompt to save the board layout.");
        quitButton.addActionListener(new miscListener(model,infolabel));

        //Create filler sizes
        Dimension groupTitleSpacing = new Dimension(900, 7);
        Dimension intraGroupSpacing = new Dimension(900, 5);
        Dimension interGroupSpacing = new Dimension(900, 70);

        //Create button size
        Dimension buttonSize = new Dimension(200, 30);

        //Set the button sizes
        startButton.setPreferredSize(buttonSize);
        tickButton.setPreferredSize(buttonSize);
        loadModelButton.setPreferredSize(buttonSize);
        reloadModelButton.setPreferredSize(buttonSize);
        buildButton.setPreferredSize(buttonSize);
        quitButton.setPreferredSize(buttonSize);

        //Add buttons to frame
        //Main Control group
        this.add(new Box.Filler(intraGroupSpacing, intraGroupSpacing, intraGroupSpacing));
        this.add(mainControlLabel);
        this.add(new Box.Filler(groupTitleSpacing, groupTitleSpacing, groupTitleSpacing));
        this.add(startButton);
        this.add(new Box.Filler(intraGroupSpacing, intraGroupSpacing, intraGroupSpacing));
        this.add(tickButton);
        this.add(new Box.Filler(interGroupSpacing, interGroupSpacing, interGroupSpacing));

        //Model Operations group
        this.add(modelOpsLabel);
        this.add(new Box.Filler(groupTitleSpacing, groupTitleSpacing, groupTitleSpacing));
        this.add(loadModelButton);
        this.add(new Box.Filler(intraGroupSpacing, intraGroupSpacing, intraGroupSpacing));
        this.add(reloadModelButton);
        this.add(new Box.Filler(interGroupSpacing, interGroupSpacing, interGroupSpacing));

        //File Operations group
        this.add(fileOpsLabel);
        this.add(new Box.Filler(groupTitleSpacing, groupTitleSpacing, groupTitleSpacing));
        this.add(buildButton);
        this.add(new Box.Filler(intraGroupSpacing, intraGroupSpacing, intraGroupSpacing));
        this.add(quitButton);
    }
}
