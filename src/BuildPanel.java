import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.util.Hashtable;
import java.util.Objects;

public class BuildPanel extends JPanel {
    private GizmoView view;
    private JPanel addComponentPanel;
    private JPanel editComponentPanel;
    private JPanel physicsPanel;
    private JPanel connectionPanel;
    private JPanel miscPanel;
    private static final Dimension GROUP_SIZE = new Dimension(300, 70);
    fakeModel model;
    private JSlider frictionDistance;
    private JSlider frictionTime;
    private JSlider gravity;

    private void populateAddComponentPanel(ActionListener boardModifier) {
        Dimension buttonSize = new Dimension(215, 30);

        addComponentPanel = new JPanel();
        addComponentPanel.setPreferredSize(new Dimension(300, 60));
        addComponentPanel.setLayout(new FlowLayout());
        addComponentPanel.setBackground(Color.LIGHT_GRAY);

        JLabel addComponentLabel = new JLabel("Add Components");
        addComponentLabel.setPreferredSize(new Dimension(290, 14));
        addComponentLabel.setHorizontalAlignment(SwingConstants.CENTER);
        addComponentLabel.setVerticalAlignment(SwingConstants.CENTER);

        JButton addBallButton = new JButton("Add ball");
        addBallButton.setPreferredSize(buttonSize);
        addBallButton.setToolTipText("Click button once to activate, then click the board to add a ball on the cursor.");

        JButton addGizmoButton = new JButton("Add gizmo");
        addGizmoButton.setPreferredSize(new Dimension(110, 30));
        addGizmoButton.setToolTipText("Click button once to activate, then click the board to add the selected gizmo on the cursor.");

        String[] gizmoStrings = {"Square", "Circle", "Triangle", "Absorber", "Left flipper", "Right flipper","Teleporter"};
        JComboBox gizmoToPlaceDropDown = new JComboBox(gizmoStrings);
        gizmoToPlaceDropDown.setPreferredSize(new Dimension(100, 30));
        gizmoToPlaceDropDown.setSelectedIndex(0);
        gizmoToPlaceDropDown.setToolTipText("A square gizmo that changes colour when activated.");

        Dimension topSpacing = new Dimension(300, 5);
        addComponentPanel.add(addComponentLabel);
        //addComponentPanel.add(new Box.Filler(topSpacing, topSpacing, topSpacing));
        addComponentPanel.add(addBallButton);
        //addComponentPanel.add(new Box.Filler(genericSpacing, genericSpacing, genericSpacing));
        addComponentPanel.add(addGizmoButton);
        addComponentPanel.add(gizmoToPlaceDropDown);

//        this.add(addComponentLabel);
//        this.add(addBallButton);
//        this.add(addGizmoButton);
//        this.add(gizmoToPlaceDropDown);
        addBallButton.addActionListener(boardModifier);
        addGizmoButton.addActionListener(boardModifier);
        gizmoToPlaceDropDown.addActionListener(boardModifier);

        if (Objects.equals(gizmoToPlaceDropDown.getSelectedItem(), "Absorber")) {
            System.out.println("YES");
        }

    }

    private void populateEditComponentPanel(ActionListener boardModifier) {
        editComponentPanel = new JPanel();
        editComponentPanel.setBackground(Color.lightGray);
        editComponentPanel.setPreferredSize(new Dimension(300, 60));
        //editComponentPanel.setLayout(new GridLayout(2, 2));

        JLabel editComponentLabel = new JLabel("Edit Components");
        editComponentLabel.setPreferredSize(new Dimension(290, 14));
        editComponentLabel.setHorizontalAlignment(SwingConstants.CENTER);
        editComponentLabel.setVerticalAlignment(SwingConstants.CENTER);

        JButton rotateButton = new JButton("Rotate");
        rotateButton.setToolTipText("Click a gizmo to rotate it 90° clockwise.");
        JButton deleteButton = new JButton("Delete");
        deleteButton.setToolTipText("Click a gizmo to delete it.");
        JButton moveButton = new JButton("Move");
        moveButton.setToolTipText("Click a gizmo to choose which one to move, then click where you would like it to go.");
        JButton clearBoardButton = new JButton("Clear Board");
        clearBoardButton.setToolTipText("Click to clear ALL balls and gizmos from the board. (!)");
        JButton colorButton = new JButton("Change Colour");
        colorButton.setToolTipText("Click a gizmo to change it's base colour via a colour picker.");
        editComponentPanel.add(editComponentLabel);
        editComponentPanel.add(rotateButton);
        editComponentPanel.add(deleteButton);
        editComponentPanel.add(moveButton);
        editComponentPanel.add(clearBoardButton);
        editComponentPanel.add(colorButton);


        //CONTROLLER SECTION
        clearBoardButton.addActionListener(boardModifier);
        rotateButton.addActionListener(boardModifier);
        moveButton.addActionListener(boardModifier);
        deleteButton.addActionListener(boardModifier);
        colorButton.addActionListener(boardModifier);
    }

    private void populatePhysicsPanel(fakeModel model) {
        physicsPanel = new JPanel();
        physicsPanel.setPreferredSize(new Dimension(300, 110));
        physicsPanel.setBackground(Color.lightGray);
        physicsPanel.setLayout(new GridLayout(3, 2));

        Dimension sliderSize = new Dimension(200, 40);

//        JLabel physicsLabel = new JLabel("Physics");
//        physicsLabel.setPreferredSize(new Dimension(290, 14));
//        physicsLabel.setHorizontalAlignment(SwingConstants.CENTER);
//        physicsLabel.setVerticalAlignment(SwingConstants.CENTER);

        JLabel frictionTimeLabel = new JLabel("Friction over time");
        JTextField frictionTimeField = new JTextField();
        //We divide these by 1000 in the listener to get the correct value
        final int friction_min = 1;
        final int friction_max = 100;
        int friction_time_default = (int) (Double.parseDouble(model.getFrictionOverTime()) * 1000);
        Hashtable labelTable = new Hashtable();
        labelTable.put(1, new JLabel("0.001"));
        //labelTable.put(25, new JLabel("0.025"));
        labelTable.put(50, new JLabel("0.05"));
        //labelTable.put(75, new JLabel("0.075"));
        labelTable.put(100, new JLabel("0.1"));
        frictionTime = new JSlider(JSlider.HORIZONTAL, friction_min, friction_max, friction_time_default);
        frictionTime.setToolTipText("Sets the rate at which balls are slowed by friction based on time.");
        frictionTime.setBackground(Color.lightGray);
//        frictionTime.setPreferredSize(sliderSize);
        frictionTime.setLabelTable(labelTable);
        frictionTime.setMajorTickSpacing(25);
        frictionTime.setMinorTickSpacing(5);
        frictionTime.setPaintTicks(true);
        frictionTime.setPaintLabels(true);
        JLabel frictionDistanceLabel = new JLabel("Friction over distance");
        JTextField frictionDistanceField = new JTextField();
        JLabel gravityLabel = new JLabel("Gravity strength");
        JTextField gravityField = new JTextField();

        //physicsPanel.add(physicsLabel);

        physicsPanel.add(frictionTimeLabel);
        physicsPanel.add(frictionTime);
        frictionTime.addChangeListener(new FrictionTimeSliderListener(model));
        frictionTimeField.setText(model.getFrictionOverTime());


        int friction_distance_default = (int) (Double.parseDouble(model.getFrictionOverDistance()) * 1000);
        labelTable = new Hashtable();
        labelTable.put(1, new JLabel("0.001"));
        //labelTable.put(25, new JLabel("0.025"));
        labelTable.put(50, new JLabel("0.05"));
        //labelTable.put(75, new JLabel("0.075"));
        labelTable.put(100, new JLabel("0.1"));
        frictionDistance = new JSlider(JSlider.HORIZONTAL, friction_min, friction_max, friction_distance_default);
        frictionDistance.setToolTipText("Sets the rate at which balls are slowed by friction based on how fast they are going.");
        frictionDistance.setBackground(Color.lightGray);
//        frictionDistance.setPreferredSize(sliderSize);
        frictionDistance.setLabelTable(labelTable);
        frictionDistance.setMajorTickSpacing(25);
        frictionDistance.setMinorTickSpacing(5);
        frictionDistance.setPaintTicks(true);
        frictionDistance.setPaintLabels(true);
        physicsPanel.add(frictionDistanceLabel);
        physicsPanel.add(frictionDistance);
        frictionDistanceField.setText(model.getFrictionOverDistance());

        frictionDistance.addChangeListener(new FrictionDistanceSliderListener(model));

        final int gravity_min = 0;
        final int gravity_max = 50;
        int gravity_default = (int) (Double.parseDouble(model.getGravity()));
        labelTable = new Hashtable();
        labelTable.put(0, new JLabel("1"));
        labelTable.put(10, new JLabel("10"));
        labelTable.put(20, new JLabel("20"));
        labelTable.put(30, new JLabel("30"));
        labelTable.put(40, new JLabel("40"));
        labelTable.put(50, new JLabel("50"));

        gravity = new JSlider(JSlider.HORIZONTAL, gravity_min, gravity_max, gravity_default);
        gravity.setToolTipText("Sets the rate at which the ball is accelerated towards the ground.");
        gravity.setBackground(Color.lightGray);
//        gravity.setPreferredSize(sliderSize);
        gravity.setLabelTable(labelTable);
        gravity.setMajorTickSpacing(10);
        gravity.setMinorTickSpacing(5);
        gravity.setPaintTicks(true);
        gravity.setPaintLabels(true);

        physicsPanel.add(gravityLabel);
        physicsPanel.add(gravity);
        gravityField.setText(model.getGravity());
        gravity.addChangeListener(new GravitySliderListener(model));
    }

    private void populateConnectionPanel(ActionListener boardModifier) {
        connectionPanel = new JPanel();
        connectionPanel.setPreferredSize(new Dimension(300, 50));
        connectionPanel.setBackground(Color.lightGray);
        //connectionPanel.setLayout(new GridLayout(2, 2));

        JLabel connectionsLabel = new JLabel("Connections");
        connectionsLabel.setPreferredSize(new Dimension(290, 14));
        connectionsLabel.setHorizontalAlignment(SwingConstants.CENTER);
        connectionsLabel.setVerticalAlignment(SwingConstants.CENTER);

        JButton connectGizmoButton = new JButton("Connect gizmo");
        connectGizmoButton.setToolTipText("First click the gizmo that you want to connect, then click the gizmo that you want to connect it to. (Connections are one-way!)");
        JButton disconnectGizmoButton = new JButton("Disconnect gizmo");
        disconnectGizmoButton.setToolTipText("First click the gizmo that you want to disconnect, then click the gizmo that you want to disconnect it from. (Removing connections is only one-way!)");
        JButton connectKeyButton = new JButton("Connect key");
        connectKeyButton.setToolTipText("First click the gizmo you want to connect to, then press the key on the keyboard you want to connect to it.");
        JButton disconnectKeyButton = new JButton("Disconnect key");
        disconnectKeyButton.setToolTipText("Click a gizmo to be prompted to remove ALL key connections from it.");
        connectionPanel.add(connectionsLabel);
        connectionPanel.add(connectGizmoButton);
        connectionPanel.add(disconnectGizmoButton);
        connectionPanel.add(connectKeyButton);
        connectionPanel.add(disconnectKeyButton);
        disconnectKeyButton.addActionListener(boardModifier);
        connectGizmoButton.addActionListener(boardModifier);
        connectKeyButton.addActionListener(boardModifier);
        disconnectGizmoButton.addActionListener(boardModifier);
    }

    private void populateMiscPanel(fakeModel model,JLabel infoLabel,ActionListener boardModifier) {
        miscPanel = new JPanel();
        miscPanel.setPreferredSize(new Dimension(300, 50));
        miscPanel.setBackground(Color.lightGray);
        //miscPanel.setLayout(new GridLayout(2, 2));

        JLabel miscLabel = new JLabel("Miscellaneous");
        miscLabel.setPreferredSize(new Dimension(290, 14));
        miscLabel.setHorizontalAlignment(SwingConstants.CENTER);
        miscLabel.setVerticalAlignment(SwingConstants.CENTER);

        JButton loadModelButton = new JButton("Load model");
        JButton toggleAudioButton = new JButton("Toggle Audio");
        toggleAudioButton.setToolTipText("Click to toggle audio on and off!");
        loadModelButton.setToolTipText("Select a file from the file system to load a board setup from.");
        JButton reloadModelButton = new JButton("Reload model");
        reloadModelButton.setToolTipText("Reloads the board to it's original setup.");
        JButton quitButton = new JButton("Quit");
        quitButton.setToolTipText("Quits the game, after a prompt to save the board layout.");
        JButton runModeButton = new JButton("Run mode");
        runModeButton.setToolTipText("Switches to run mode, so that you can start playing.");
        JButton queryButton = new JButton("Query");
        queryButton.setToolTipText("Click this, then click on a gizmo to see the gizmos and keys that it's connected to.");
        miscPanel.add(miscLabel);
        miscPanel.add(loadModelButton);
        miscPanel.add(reloadModelButton);
        miscPanel.add(quitButton);
        miscPanel.add(runModeButton);
        miscPanel.add(toggleAudioButton);
        miscPanel.add(queryButton);



        reloadModelButton.addActionListener(new ReloadModelListener( model));
        loadModelButton.addActionListener(new LoadModelListener(model,  view));
        ActionListener miscListener = new miscListener(model, infoLabel);
        quitButton.addActionListener(miscListener);
        runModeButton.addActionListener(miscListener);
        toggleAudioButton.addActionListener(miscListener);
        queryButton.addActionListener(boardModifier);
    }

    public BuildPanel(fakeModel model, BoardModifyListener boardModifyListener, GizmoView view,JLabel infolabel) {
        this.model = model;
        this.view = view;

        this.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
        populateAddComponentPanel(boardModifyListener);
        populateEditComponentPanel(boardModifyListener);
        populatePhysicsPanel(model);
        populateConnectionPanel(boardModifyListener);
        populateMiscPanel(model,infolabel,boardModifyListener);


        this.add(addComponentPanel);
        //this.add(new JSeparator(SwingConstants.HORIZONTAL));
        this.add(editComponentPanel);
        //this.add(new JSeparator(SwingConstants.HORIZONTAL));
        this.add(physicsPanel);
        //this.add(new JSeparator(SwingConstants.HORIZONTAL));
        this.add(connectionPanel);
        //this.add(new JSeparator(SwingConstants.HORIZONTAL));
        this.add(miscPanel);
    }

    public void updateSliders() {
        frictionDistance.setValue((int) (Double.parseDouble(model.getFrictionOverDistance()) * 1000));
        frictionTime.setValue((int) (Double.parseDouble(model.getFrictionOverTime()) * 1000));
        gravity.setValue((int) (Double.parseDouble(model.getGravity())));
    }
}
