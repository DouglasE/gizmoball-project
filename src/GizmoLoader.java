

import Exceptions.DuplicateGizmoException;
import Exceptions.badFileException;
import Exceptions.badSelectException;
import Exceptions.noSuchConnectionException;

import java.io.*;
import java.text.ParseException;
import java.util.StringTokenizer;


public class GizmoLoader {

    private BufferedReader fileInput;

    public GizmoLoader(File file) {
        try {
            fileInput = new BufferedReader(new FileReader(file));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void readFile(fakeModel Model) throws IOException, ParseException, badFileException {
        String line;
        StringTokenizer st;

        line = fileInput.readLine();

        while (line != null) {
            st = new StringTokenizer(line, " ", false);
            while (st.hasMoreTokens()) {
                String gizmoID;
                String Type = st.nextToken();
                int XCoord;
                int YCoord;
                Gizmo gizmo;
                switch (Type) {

                    case "Triangle":
                        if(st.countTokens()  == 3) {

                            gizmoID = st.nextToken();
                            XCoord = Integer.parseInt(st.nextToken());
                            YCoord = Integer.parseInt(st.nextToken());
                            checkCoords(XCoord, YCoord);
                            gizmo = new Triangle(gizmoID, XCoord, YCoord);
                            try {
                                Model.addGizmo(gizmo, gizmoID);
                            } catch (DuplicateGizmoException e) {
                                System.out.println("I can already see this gizmo!");
                            } catch (badSelectException e) {
                                System.out.println("Invalid Co-ordinates!");
                            }
                            break;
                        }break;
                    case "Square":
                        if(st.countTokens()  == 3) {

                            gizmoID = st.nextToken();

                            XCoord = Integer.parseInt(st.nextToken());
                            YCoord = Integer.parseInt(st.nextToken());
                            checkCoords(XCoord, YCoord);
                            gizmo = new Square(gizmoID, XCoord, YCoord);
                            try {
                                Model.addGizmo(gizmo, gizmoID);
                            } catch (DuplicateGizmoException e) {
                                System.out.println("I can already see this gizmo!");
                            } catch (badSelectException e) {
                                System.out.println("Invalid Co-ordinates!");
                            }
                            break;
                        }break;
                    case "Teleporter":
                        if(st.countTokens()  == 3) {

                            gizmoID = st.nextToken();

                            XCoord = Integer.parseInt(st.nextToken());
                            YCoord = Integer.parseInt(st.nextToken());
                            checkCoords(XCoord, YCoord);
                            gizmo = new TelePorter(gizmoID, XCoord, YCoord);
                            try {
                                Model.addGizmo(gizmo, gizmoID);
                            } catch (DuplicateGizmoException e) {
                                System.out.println("I can already see this gizmo!");
                            } catch (badSelectException e) {
                                System.out.println("Invalid Co-ordinates!");
                            }
                            break;
                        }break;
                    case "Circle":
                        if(st.countTokens()  == 3) {

                            gizmoID = st.nextToken();
                            XCoord = Integer.parseInt(st.nextToken());
                            YCoord = Integer.parseInt(st.nextToken());
                            checkCoords(XCoord, YCoord);
                            gizmo = new CircleGizmo(gizmoID, XCoord, YCoord);
                            try {
                                Model.addGizmo(gizmo, gizmoID);
                            } catch (DuplicateGizmoException e) {
                                System.out.println("I can already see this gizmo!");
                            } catch (badSelectException e) {
                                System.out.println("Invalid Co-ordinates!");
                            }
                            break;
                        }break;
                    case "LeftFlipper":
                        if(st.countTokens()  == 3) {

                            gizmoID = st.nextToken();
                            XCoord = Integer.parseInt(st.nextToken());
                            YCoord = Integer.parseInt(st.nextToken());
                            checkCoords(XCoord, YCoord);
                            gizmo = new LeftFlipper(gizmoID, XCoord, YCoord);
                            try {
                                Model.addGizmo(gizmo, gizmoID);
                            } catch (DuplicateGizmoException e) {
                                System.out.println("I can already see this gizmo!");
                            } catch (badSelectException e) {
                                System.out.println("Invalid Co-ordinates!");
                            }
                            break;
                        }break;
                    case "RightFlipper":

                        if(st.countTokens()  == 3) {

                            gizmoID = st.nextToken();
                            XCoord = Integer.parseInt(st.nextToken());
                            YCoord = Integer.parseInt(st.nextToken());
                            checkCoords(XCoord, YCoord);
                            gizmo = new RightFlipper(gizmoID, XCoord, YCoord);
                            try {
                                Model.addGizmo(gizmo, gizmoID);
                            } catch (DuplicateGizmoException e) {
                                System.out.println("I can already see this gizmo!");
                            } catch (badSelectException e) {
                                System.out.println("Invalid Co-ordinates!");
                            }
                            break;
                        }break;
                    case "Absorber":
                        if(st.countTokens()  == 5) {


                            gizmoID = st.nextToken();
                            XCoord = Integer.parseInt(st.nextToken());
                            YCoord = Integer.parseInt(st.nextToken());
                            checkCoords(XCoord, YCoord);
                            int X2Coord = Integer.parseInt(st.nextToken());
                            int Y2Coord = Integer.parseInt(st.nextToken());
                            gizmo = new Absorber(gizmoID, XCoord, YCoord, X2Coord, Y2Coord);
                            try {
                                Model.addGizmo(gizmo, gizmoID);
                            } catch (DuplicateGizmoException e) {
                                System.out.println("I can already see this gizmo!");
                            } catch (badSelectException e) {
                                System.out.println("Invalid Co-ordinates!");
                            }
                            break;
                        }
                        break;
                    case "Rotate":
                        gizmoID = st.nextToken();
                        Model.rotateGizmo(gizmoID);
                        break;
                    case "KeyConnect":
                        st.nextToken();
                        String keyNum1 = st.nextToken();
                        String upOrdown = st.nextToken();

                        gizmoID = st.nextToken();
                        if (!Model.hasGizmo(gizmoID)) {
                            throw new badFileException("Keyconnect to a gizmo that does not exist: " + gizmoID);
                        }
                        Model.addKeyConnect(gizmoID, keyNum1, upOrdown);
                        break;
                    case "Ball":
                        String BallID = st.nextToken();
                        if(st.countTokens()  == 4){


                        float X1 = Float.parseFloat(st.nextToken());
                        float Y1 = Float.parseFloat(st.nextToken());
                        float X2 = Float.parseFloat(st.nextToken());
                        float Y2 = Float.parseFloat(st.nextToken());
                        Model.addBall(BallID, X1, Y1, X2, Y2);
                        break;
                             }
                        break;
                    case "Connect":
                        gizmoID = st.nextToken();
                        if (!Model.hasGizmo(gizmoID)) {
                            if (!gizmoID.equalsIgnoreCase("outerwalls")) {
                                throw new badFileException("Connect to a gizmo that does not exist: " + gizmoID);
                            }
                        }
                        String Consumer = st.nextToken();
                        if (!Model.hasGizmo(Consumer)) {
                            if (!Consumer.equalsIgnoreCase("outerwalls")) {
                                throw new badFileException("Connect to a gizmo that does not exist: " + Consumer);
                            }
                        }
                      try {
                        Model.addConnection(gizmoID, Consumer);
                      } catch (badSelectException e) {
                        System.out.println("Empty string or invalid ID");
                      } catch (noSuchConnectionException e) {
                          System.out.println(e.getMessage());
                      }
                        break;
                    case "Delete":
                        gizmoID = st.nextToken();
                        try {
                            Model.removeGizmo(gizmoID);
                        } catch (noSuchConnectionException e) {
                            System.out.println("Bad removal attempt");
                        }
                    case "Move":
                        if(st.countTokens()  != 3){
                            throw new badFileException("Invalid move line");
                        }
                        gizmoID = st.nextToken();
                        String firstOfPair = st.nextToken();
                        String secondOfPair = st.nextToken();
                        try {
                            Model.moveGizmo(gizmoID, Double.parseDouble(firstOfPair), Double.parseDouble(secondOfPair));
                        } catch (badSelectException e) {
                            System.out.println("Something's already there...");
                        }
                        break;
                    case "Friction":
                        if(st.countTokens()  == 2) {
                            String firstofPair = st.nextToken();
                            String secondofPair = st.nextToken();
                            float frictionOverTime = Float.parseFloat(firstofPair);
                            float frictionOverDistance = Float.parseFloat(secondofPair);
                            if (frictionOverTime < Float.parseFloat("0.001") || frictionOverTime > Float.parseFloat("0.1")) {
                                throw new badFileException("Friction has an invalid value of " + firstofPair);
                            }

                            if (frictionOverDistance < Float.parseFloat("0.001") || frictionOverDistance > Float.parseFloat("0.1")) {
                                throw new badFileException("Friction has an invalid value of " + secondofPair);
                            }
                            Model.setFrictionOverTime(Float.parseFloat(firstofPair));
                            Model.setFrictionOverDistance(Float.parseFloat(secondofPair));
                            break;
                        }
                        break;
                    case "Gravity":
                        if(st.countTokens()  == 1) {

                            String grav = st.nextToken();
                            float gravity = Float.parseFloat(grav);
                            System.out.println();
                            //Check min and max
                            if (gravity < 0 || gravity > 50) {
                                throw new badFileException("Gravity has an invalid value of " + grav);
                            }
                            Model.setGravity(Float.parseFloat(grav));
                            break;
                        }
                    default:
                        break;
                }
            }
            line = fileInput.readLine();

        }
    }

    private void checkCoords(int XCoord, int YCoord) throws badFileException {
        if (XCoord < 0 || XCoord > 20 || YCoord < 0 || YCoord > 20) {
            throw new badFileException("Bad Coordinates for gizmo in file");
        }
    }
}
