import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

public class miscListener implements ActionListener {

  private fakeModel model;
  private JLabel infoLabel;
  public miscListener(fakeModel Model, JLabel infoLabel){
    this.infoLabel = infoLabel;
    this.model = Model;
  }
  @Override
  public void actionPerformed(ActionEvent e) {
    if (e.getActionCommand().equalsIgnoreCase("Quit")) {

      int reply = JOptionPane.showConfirmDialog(null, "Quit and save?", "Quit and save?", JOptionPane.YES_NO_CANCEL_OPTION);
      if (reply == JOptionPane.YES_OPTION) {
        String s = JOptionPane.showInputDialog("Input the name of the file you'd like it to be saved as.");
        if(s == null || s.equalsIgnoreCase("") || s.replaceAll(" ","").equalsIgnoreCase("")) {
            JOptionPane.showConfirmDialog(null,"Enter a non empty filename","Bad filename", JOptionPane.OK_CANCEL_OPTION,JOptionPane.ERROR_MESSAGE);
            return;
        }
      GizmoSaver saver = new GizmoSaver(s + ".txt");
        try {
          saver.saveFile(model);
        } catch (IOException e1) {
          JOptionPane.showMessageDialog(null, "FAILURE OF SAVING SYSTEM!");
        }
        System.exit(0);
      }
      else if(reply == JOptionPane.CANCEL_OPTION){

      }
      else{
        System.exit(0);
      }
    }
    else if(e.getActionCommand().equalsIgnoreCase("Run Mode")){
      GizmoView.setToRun();
      infoLabel.setText("Ready to run!");
      infoLabel.setBackground(Color.green);
      infoLabel.setBorder(BorderFactory.createBevelBorder(1));
      model.setAllModesFalse();
    }
    else if(e.getActionCommand().equalsIgnoreCase("Reset Model")){
      model.reset();
      infoLabel.setText("Model reset!");
      infoLabel.setBackground(Color.green);
      infoLabel.setBorder(BorderFactory.createBevelBorder(1));
    }
    else if(e.getActionCommand().equalsIgnoreCase("Toggle Audio")){
      model.toggleAudio();
      if (model.isAudioOn()) {
          infoLabel.setText("Audio enabled!");
          infoLabel.setBackground(Color.green);
          infoLabel.setBorder(BorderFactory.createBevelBorder(1));
      } else {
          infoLabel.setText("Audio disabled!");
          infoLabel.setBackground(Color.red);
          infoLabel.setBorder(BorderFactory.createBevelBorder(1));
      }
    }
  }
}

