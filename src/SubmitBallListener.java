import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SubmitBallListener implements ActionListener {
    fakeModel model;
    float x;
    float y;
    JSlider xVeloSlider;
    JSlider yVeloSlider;
    JDialog jDialog;

    public SubmitBallListener(fakeModel model, float x, float y, int pixelsPerGridBox, JSlider xVeloSlider, JSlider yVeloSlider, JDialog jDialog) {
        this.model = model;
        this.x = x / pixelsPerGridBox;
        this.y = y / pixelsPerGridBox;
        this.xVeloSlider = xVeloSlider;
        this.yVeloSlider = yVeloSlider;
        this.jDialog = jDialog;
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        int ballId = model.generateGizmoID("ball");
        float xVelo = (float) xVeloSlider.getValue() / 10;
        float yVelo = (float) yVeloSlider.getValue() / 10;
        model.addBall("B" + ballId, x, y, xVelo, yVelo);
        jDialog.dispose();
    }
}
