import Exceptions.noSuchConnectionException;
import physics.Circle;
import physics.LineSegment;

import java.awt.*;
import java.util.List;
import java.util.NoSuchElementException;

public interface Gizmo {




Color getColor();
String getID();
void rotate();
int getX();
int getY();
String getType();
ROTATION getRotation();
boolean addGizmoConnection(String gizmoToConnect);
List<String> getGizmoConnections();

List<LineSegment> getLineSegments();
List<Circle> getCircles();

//Return an array of numbers, in the order they are required!
int[] getBoardX();
int[] getBoardY();
//Return an array of numbers, in the order they are required!

boolean addKeyConnect(String keyVal, String UpOrDown);
List<String> getKeyConnections();
void trigger();
void move(int x, int y);
void removeConnection(String remove) throws noSuchConnectionException;
void removeKeyConnects();
void reset();
void setColor(Color color);
}
