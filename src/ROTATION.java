public enum ROTATION {
    NONE(0),TOPRIGHT(1), TOPLEFT(2), BOTTOMLEFT(3),BOTTOMRIGHT(4);
    private int numVal;
    ROTATION(int numVal) {
        this.numVal = numVal;
    }

    public int getNumVal() {
        return numVal;
    }
}