
import Exceptions.noSuchConnectionException;
import physics.Circle;
import physics.LineSegment;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class Square  implements  Gizmo{
    private final String ID;
    private int XCoord;
    private int YCoord;
    private LineSegment TOP;
    private LineSegment Bottom;
    private LineSegment Left;
    private LineSegment Right;
    private ROTATION rotation;
    private List<LineSegment> lines;
    private List<Circle> circles;
    private List<String> keyConnections;
    private Color color;
    private Color defaultColor;
    private Color triggerColor;
    private List<String> gizmoConnections;

    public Square(String ID,int X, int Y){
      rotation = ROTATION.NONE;
        gizmoConnections = new ArrayList<>();
        keyConnections = new ArrayList<>();
        defaultColor = Color.red;
        triggerColor = Color.green;
        color =defaultColor;
        lines = new ArrayList<>();
        this.ID = ID;
        this.XCoord  = X;
        this.YCoord = Y;
        //+1 BECAUSE WE WANT THE OTHER CORNER, ONE L ALONG!
        TOP = new LineSegment(X,Y,(X)+1,(Y));
       Bottom = new LineSegment(X,(Y)+1,(X),(Y));
        Left = new LineSegment(X+1,Y,X+1,(Y)+1);
        Right = new LineSegment(X,Y+1,X+1,Y+1);

      Circle ONE  = new Circle(X,Y,0);
      Circle TWO  = new Circle(X,Y+1,0);
      Circle THREE  = new Circle(X+1,Y,0);
      Circle FOUR  = new Circle(X+1,Y+1,0);

      circles = new ArrayList<>();
      circles.add(ONE);
      circles.add(TWO);
      circles.add(THREE);
      circles.add(FOUR);

      lines.add(TOP);
        lines.add(Bottom);
        lines.add(Left);
        lines.add(Right);

    }
    @Override
    public String getID() {
        return ID;
    }
    @Override
    public void rotate() {

    }

    @Override
    public int getX() {
        return XCoord;
    }

    @Override
    public int getY() {
        return YCoord;
    }

    @Override
    public String getType() {
        return "Square";
    }

    @Override
    public ROTATION getRotation() {
        return rotation;
    }

    @Override
    public boolean addGizmoConnection(String gizmoToConnect) {
        if(gizmoConnections.contains(gizmoToConnect)){
            return false;
        }
        else{
            return gizmoConnections.add(gizmoToConnect);
        }
    }

    @Override
    public List<String> getGizmoConnections() {
        return gizmoConnections;
    }
    @Override
    public int[] getBoardX() {
        return new int[0];
    }

    @Override
    public int[] getBoardY() {
        return new int[0];
    }


    @Override
    public String toString(){
        StringBuilder s = new StringBuilder("Square  " + ID + " " + XCoord + " " + YCoord);
        for(String keyConnectString : keyConnections){
            String[] strings = keyConnectString.split("\\.",104);
            s.append("\nKeyConnect");
            s.append(" key ").append(strings[1]);
            s.append(" ").append(strings[2]);
            s.append(" ").append(this.getID());

        }
      for(String consumer : gizmoConnections){
        s.append("\nConnect").append(" ").append(this.getID()).append(" ").append(consumer);
      }
        return  s.toString();
    }

    @Override
    public Color getColor() {
        return color;
    }
    @Override
    public List<LineSegment> getLineSegments(){
        return lines;
    }
   @Override
    public List<Circle> getCircles(){
        return circles;
    }
    @Override
    public List<String> getKeyConnections() {
        return new ArrayList<>(keyConnections);
    }

    @Override
    public void trigger() {
        if(this.getColor() == defaultColor){
            this.color = triggerColor;
        }else{
            this.color = defaultColor;
        }

    }

  @Override
  public void move(int X, int Y) {
    this.XCoord = X;
    this.YCoord = Y;
    TOP = new LineSegment(X,Y,(X)+1,(Y));
    Bottom = new LineSegment(X,(Y)+1,(X),(Y));
    Left = new LineSegment(X+1,Y,X+1,(Y)+1);
    Right = new LineSegment(X,Y+1,X+1,Y+1);

    Circle ONE  = new Circle(X,Y,0);
    Circle TWO  = new Circle(X,Y+1,0);
    Circle THREE  = new Circle(X+1,Y,0);
    Circle FOUR  = new Circle(X+1,Y+1,0);

    circles = new ArrayList<>();
    circles.add(ONE);
    circles.add(TWO);
    circles.add(THREE);
    circles.add(FOUR);

    lines = new ArrayList<>();
    lines.add(TOP);
    lines.add(Bottom);
    lines.add(Left);
    lines.add(Right);
  }

  @Override
    public boolean addKeyConnect(String key,String UpOrDown) {
    String keyString  = "key" + "." + key + "." + UpOrDown;
    if(!keyConnections.contains(keyString)) {
      keyConnections.add(keyString);
      return true;
    }else{
      return  false;
    }
    }
    @Override
    public int hashCode(){
        return ID.hashCode();
    }
  @Override
  public void removeConnection(String remove) throws noSuchConnectionException {
    if(!gizmoConnections.contains(remove)){
      throw new noSuchConnectionException("Connection does not exist!");
    }
    gizmoConnections.remove(remove);

  }

    @Override
    public void removeKeyConnects() {
        keyConnections.clear();
    }

  @Override
  public void reset() {

      this.defaultColor = Color.red;
      this.color = Color.red;
  }


  @Override
    public boolean equals(Object o){
      if(o == null){
        return false;
      }
        if(!(o instanceof  Square)){
            return  false;
        }

        return this.getID().equals(((Square) o).getID()) && this.getX() == ((Square) o).getX() && this.getY() == ((Square) o).getY();
    }
  @Override
  public void setColor(Color color) {
    this.defaultColor =color;
    this.color = color;
  }
}
