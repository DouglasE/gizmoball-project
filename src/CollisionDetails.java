

import physics.Geometry;
import physics.Vect;
/**
 * @author Murray Wood Demonstration of MVC and MIT Physics Collisions 2014
 */

public  class CollisionDetails {
  private double tuc;
  private Vect velo;

  private Gizmo gizmoHit;
  private Ball ballHit;
  private Geometry.VectPair v;
  public CollisionDetails(double t, Vect v, Gizmo hit) {
    tuc = t;
    velo = v;
    ballHit = null;
    gizmoHit = hit;
  }
  public CollisionDetails(double t, Geometry.VectPair v, Ball hit) {
    tuc = t;
    this.v = v;
    ballHit = hit;
    gizmoHit = null;
  }
  public Geometry.VectPair getPair(){
    return v;
  }
  public double getTuc() {
    return tuc;
  }
  public Ball getBallHit(){
    return  ballHit;
  }
  public Vect getVector() {
    return velo;
  }
  public Gizmo getGizmoHit() {
    return gizmoHit;
  }

}
