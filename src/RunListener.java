import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class RunListener implements ActionListener {
  private fakeModel model;
  private Timer t;
  public RunListener(fakeModel model){
    this.model = model;


    t = new Timer(10, this);
    t.start();
  }

  public Timer getTimer(){
    return t;
  }
  @Override
  public void actionPerformed(ActionEvent e) {

    if(e.getSource() == t){
      model.moveBalls();
      for(Gizmo g : model.getGizmos()) {
        if(g instanceof LeftFlipper || g instanceof RightFlipper) {
          g.trigger();
        }
      }
    }
    else if (e.getActionCommand().equalsIgnoreCase("Stop")) {
      model.toggleKeys(false);
      model.purgeKeyPresses();
      t.stop();
    }
    else if (e.getActionCommand().equalsIgnoreCase("Tick")){
      model.stopAllBalls();
      model.moveBalls();
      model.startAllBalls();
    }
  }
}
