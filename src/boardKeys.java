import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;

public class boardKeys implements KeyListener {

    private fakeModel model;
    public boardKeys(fakeModel m){
        this.model = m;
    }
    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
        if(model.isAcceptingKeypresses()){
            model.addKeyPress("key"+"."+e.getKeyCode()+"."+"down");
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        if(model.isAcceptingKeypresses()){
           model.addKeyPress("key"+"."+e.getKeyCode()+"."+"up");
        }
    }
}
