import java.io.File;
import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException {
      fakeModel model = new fakeModel();
      File file = new File("file.txt");
      model.setFile(file);
      GizmoLoader gizmoLoader = new GizmoLoader(file);
      try {
        gizmoLoader.readFile(model);
        GizmoSaver g = new GizmoSaver("savedFile.txt");
        g.saveFile(model);
      } catch (Exception e) {
        System.out.println(e.getMessage());
      }

      RunListener runner = new RunListener(model);
      GizmoView view = new GizmoView(model,runner.getTimer());


    }
}
