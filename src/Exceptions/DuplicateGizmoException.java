package Exceptions;

public class DuplicateGizmoException extends  Exception {
  public DuplicateGizmoException(String message){
    super(message);
  }
}
