package Exceptions;

public class noSuchConnectionException extends  Exception {

  public noSuchConnectionException(String message){
    super(message);
  }
}
