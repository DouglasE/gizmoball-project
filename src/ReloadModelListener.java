import Exceptions.badFileException;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.text.ParseException;

public class ReloadModelListener implements ActionListener {

    private fakeModel model;

    public ReloadModelListener( fakeModel model) {
        this.model = model;
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        File file = model.getFile();
        GizmoLoader gizmoLoader = new GizmoLoader(file);
        try {
            model.clear();
            gizmoLoader.readFile(model);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            JOptionPane.showMessageDialog(null, "File is in an invalid format");
        } catch (badFileException e) {
            if (!e.getMessage().isEmpty()) {
                JOptionPane.showMessageDialog(null, e.getMessage());

            } else {
                JOptionPane.showMessageDialog(null, "File has errors in it");
            }
        }
    }
}
