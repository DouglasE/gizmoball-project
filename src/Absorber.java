import Exceptions.noSuchConnectionException;
import physics.Circle;
import physics.LineSegment;
import physics.Vect;

import java.awt.*;
import java.util.*;
import java.util.List;

public class Absorber implements Gizmo {
  private final String ID;
  private ROTATION rotation;
  private  int XCoord;
  private  int YCoord;
  private  int X2Coord;
  private  int Y2Coord;
  private Deque<Ball> balls;
  private List<LineSegment> lines;
  private List<String> keyConnections;
  private Vect velocityToFireBall = new Vect(0, -50);
  private List<String> gizmoConnections;
  private Color color;
  private Color defaultColor;
  private Color triggerColor;

  public Absorber(String ID, int X, int Y, int X2, int Y2) {
    gizmoConnections = new ArrayList<>();
    keyConnections = new ArrayList<>();
    balls = new ArrayDeque<>();
    defaultColor = Color.magenta;
    color = defaultColor;
    triggerColor = Color.magenta;
    this.ID = ID;
    rotation = ROTATION.NONE;
    this.XCoord = X;
    this.YCoord = Y;
    this.X2Coord = X2;
    this.Y2Coord = Y2;

    LineSegment top = new LineSegment(X , Y , X2 , Y );
    LineSegment right = new LineSegment(X2, Y , X2 , Y2 );
    LineSegment bottom = new LineSegment(X, Y2, X2, Y2 );
    LineSegment left = new LineSegment(X , Y, X, Y2);

    lines = new ArrayList<>();
    lines.add(top);
    lines.add(right);
    lines.add(bottom);
    lines.add(left);
  }

  public void setVelocityToFireBall(double upSpeed) {
    this.velocityToFireBall = new Vect(0, upSpeed);
  }

  public void addBall(Ball ball) {
    balls.addLast(ball);
    //6.25 since spec says 0.25L from bottom and right of absorber right bottom corner
    final double ballOffset = 0.25;
    ball.setX(X2Coord  - ballOffset);
    ball.setY(Y2Coord - ballOffset);
    ball.setVector(new Vect(0, 0));
    ball.setActive(false);
    //Absorber is supposed to fire the ball immediately if it's connected to itself.
    if (gizmoConnections.contains(this.getID())) {
      fireBall();
    }
  }

  public void fireBall() {
    //TODO: Find a way to have ball stop colliding with absorber when fired
    if (balls.peekFirst() == null) {
      return;
    }

    balls.peekFirst().setActive(true);
    //TODO: This might be very hacky, but it does get around the issue of colliding
    //with the absorber from the inside
    balls.peekFirst().setY(YCoord -  balls.peekFirst().getRadius());
    balls.peekFirst().setVector(velocityToFireBall);
    balls.removeFirst();
  }

  @Override
  public Color getColor() {
    return color;
  }

  @Override
  public String getID() {
    return ID;
  }

  @Override
  public void rotate() {

  }

  @Override
  public int getX() {
    return XCoord;
  }

  @Override
  public int getY() {
    return YCoord;
  }

  @Override
  public String getType() {
    return "Absorber";
  }

  @Override
  public ROTATION getRotation() {
    return ROTATION.NONE;
  }

  @Override
  public boolean addGizmoConnection(String gizmoToConnect) {
      if(gizmoConnections.contains(gizmoToConnect)){
          return false;
      }
      else{
          return gizmoConnections.add(gizmoToConnect);
      }
  }

  @Override
  public List<String> getGizmoConnections() {
    return gizmoConnections;
  }

  @Override
  public List<LineSegment> getLineSegments() {

    LineSegment top = new LineSegment(this.getX() , getY() , this.getX2Coord() , getY() );
    LineSegment right = new LineSegment(this.getX2Coord(), getY() , this.getX2Coord() , getY2Coord() );
    LineSegment bottom = new LineSegment(this.getX(), this.getY2Coord(), this.getX2Coord(), getY2Coord() );
    LineSegment left = new LineSegment(this.getX() , getY(), getX(), getY2Coord());

    lines = new ArrayList<>();
    lines.add(top);
    lines.add(right);
    lines.add(bottom);
    lines.add(left);

    return lines;
  }

  @Override
  public List<Circle> getCircles() {
    List<Circle> circles = new ArrayList<>();
    Circle topLeft = new Circle(XCoord , YCoord , 0);
    Circle topRight = new Circle(X2Coord , YCoord  +1, 0);
    Circle bottomLeft = new Circle(XCoord , Y2Coord , 0);
    Circle bottomRight = new Circle(getX2Coord(), getY2Coord() , 0);

    circles.add(topLeft);
    circles.add(topRight);
    circles.add(bottomLeft);
    circles.add(bottomRight);

    return circles;
  }

  @Override
  public int[] getBoardX() {
    return new int[0];
  }

  @Override
  public int[] getBoardY() {
    return new int[0];
  }

  @Override
  public boolean addKeyConnect(String key, String UpOrDown) {
    String keyString  = "key" + "." + key + "." + UpOrDown;
    if(!keyConnections.contains(keyString)) {
      keyConnections.add(keyString);
      return true;
    }else{
      return  false;
    }

  }

  @Override
  public List<String> getKeyConnections() {
    return keyConnections;
  }

  @Override
  public void trigger() {

    fireBall();

  }

  @Override
  public void move(int x, int y) {
    int backupX = this.X2Coord - this.XCoord;
    int backupY = this.Y2Coord - this.YCoord;

  this.XCoord = x;
  this.YCoord = y;
  this.X2Coord = x + backupX;
  this.Y2Coord = y + backupY;
  }

  @Override
  public void removeConnection(String remove) throws noSuchConnectionException {
    if(!gizmoConnections.contains(remove) ){
      throw new noSuchConnectionException("Connection does not exist Or trying to disconnect Absorber from itself!");
    }
    gizmoConnections.remove(remove);
  }

  @Override
  public void removeKeyConnects() {
    keyConnections.clear();
  }

  @Override
  public void reset() {
    this.color = Color.magenta;
    balls.clear();
  }

  @Override
  public void setColor(Color color)

  {
    this.color = color;
    this.defaultColor =color;
  }
  public String toString() {
    StringBuilder s = new StringBuilder("Absorber " + ID + " " + XCoord + " " + YCoord + " " + X2Coord + " " + Y2Coord);
    for (String keyConnectString : keyConnections) {
      String[] strings = keyConnectString.split("\\.", 104);
      System.out.println(Arrays.toString(strings));
      s.append("\nKeyConnect");
      s.append(" key ").append(strings[1]);
      s.append(" ").append(strings[2]);
      s.append(" ").append(this.getID());

    }
    for(String consumer : gizmoConnections){
      s.append("\nConnect").append(" ").append(this.getID()).append(" ").append(consumer);
    }
    return s.toString();
  }

  @Override
  public int hashCode() {
    return ID.hashCode();
  }

  public int getX2Coord() {
    return X2Coord;
  }

  public int getY2Coord() {
    return Y2Coord;
  }

}
