import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class StartListener implements ActionListener {

    private fakeModel model;
    JButton button;
    private Board board;
    private Timer t;
    public StartListener(fakeModel model, JButton button, Board board,Timer t) {
        this.model = model;
        this.button = button;
        this.board = board;
        this.t = t;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        board.requestFocus();
        if (model.allBallsStopped()) {
            model.startAllBalls();
            button.setText("Stop");
            button.setToolTipText("Pauses the game.");
            model.toggleKeys(true);
            t.start();
        } else if(!model.allBallsStopped()) {
            model.stopAllBalls();
            button.setText("Start");
            button.setToolTipText("Starts the game.");
            model.toggleKeys(false);
            t.stop();
        }

    }
}
