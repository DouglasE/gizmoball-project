import javax.swing.*;
import javax.tools.JavaCompiler;
import java.awt.*;
import java.awt.event.KeyListener;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.util.Observable;
import java.util.Observer;

public class Board extends JPanel implements Observer {
  /*
  Board is 20L x 20L
  25 pixels per L
   */
  private static final int PIXELS_PER_GRID_BOX = 25;
  private static final int COLUMNS = 20;
  private static final int ROWS = 20;
  private static final int BOARD_WIDTH = ROWS * PIXELS_PER_GRID_BOX;
  private static final int BOARD_HEIGHT = COLUMNS * PIXELS_PER_GRID_BOX;
  private fakeModel model;
  private JLabel ballSpeed;


  public Board(fakeModel model) {
    KeyListener boardKeys = new boardKeys(model);
    KeyListener keys = new MagicKeyListener(boardKeys);
    this.addKeyListener(keys);
    setPreferredSize(new Dimension(500, 500));
    this.model = model;
    model.addObserver(this);
    ballSpeed = new JLabel();
    ballSpeed.setBackground(Color.black);
    ballSpeed.setForeground(Color.white);
    this.add(ballSpeed);

  }

  public void paintComponent(Graphics g) {


    super.paintComponent(g);


    Graphics2D g2 = (Graphics2D) g;

    if (!GizmoView.isrunning()) {
      //Vertical lines
      for (int i = 0; i < COLUMNS; i++) {
        g2.drawLine(i * PIXELS_PER_GRID_BOX, 0, i * PIXELS_PER_GRID_BOX, BOARD_HEIGHT);
      }

      //Horizontal lines
      for (int i = 0; i < ROWS; i++) {
        g2.drawLine(0, i * PIXELS_PER_GRID_BOX, BOARD_WIDTH, i * PIXELS_PER_GRID_BOX);
      }
      this.setBackground(Color.white);
    } else {
      this.setBackground(Color.black);
    }

    for (Gizmo gizmo : model.getGizmos()) {
      int xPos = gizmo.getX() * PIXELS_PER_GRID_BOX;
      int yPos = gizmo.getY() * PIXELS_PER_GRID_BOX;
      //Draw them
      switch (gizmo.getType()) {
        case "Circle":
          g2.setPaint(gizmo.getColor());
          Ellipse2D.Double shape = new Ellipse2D.Double(xPos, yPos, PIXELS_PER_GRID_BOX, PIXELS_PER_GRID_BOX);
          g2.draw(shape);
          g2.fill(shape);
          g2.setPaint(Color.black);
          break;
        case "Square":
          g2.setPaint(gizmo.getColor());
          g2.fillRect(xPos, yPos, PIXELS_PER_GRID_BOX, PIXELS_PER_GRID_BOX);
          g2.setPaint(Color.black);
          break;

        case "Teleporter":
          g2.setPaint(gizmo.getColor());
          g2.fillRect(xPos, yPos, PIXELS_PER_GRID_BOX, PIXELS_PER_GRID_BOX);
          g2.setPaint(Color.black);
          break;
        case "Triangle":
          int[] x = gizmo.getBoardX();
          for (int i = 0; i < x.length; i++) {
            x[i] = x[i] * PIXELS_PER_GRID_BOX;
          }
          int[] y = gizmo.getBoardY();
          for (int k = 0; k < y.length; k++) {
            y[k] = y[k] * PIXELS_PER_GRID_BOX;
          }
          g2.setPaint(gizmo.getColor());
          g2.fillPolygon(x, y, 3);
          g2.setPaint(Color.black);
          break;
        case "Absorber":
          g2.setPaint(gizmo.getColor());
          Absorber a = (Absorber) gizmo;
          g2.fillRect(xPos, yPos, (a.getX2Coord() * PIXELS_PER_GRID_BOX) - xPos, (a.getY2Coord() * PIXELS_PER_GRID_BOX) - yPos);
          break;
        case "LeftFlipper":
          g2.setPaint(gizmo.getColor());
          LeftFlipper leftFlipper = (LeftFlipper) gizmo;
          double[] xCoords = leftFlipper.getBoardXDoouble();
          double[] yCoords = leftFlipper.getBoardYDouble();
          Stroke strokeBefore = g2.getStroke();
          g2.setStroke(new BasicStroke((float) 12.5, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));

          g2.draw(new Line2D.Double((xCoords[0] * PIXELS_PER_GRID_BOX), (yCoords[0] * PIXELS_PER_GRID_BOX), (xCoords[1] * PIXELS_PER_GRID_BOX), (yCoords[1] * PIXELS_PER_GRID_BOX)));
          g2.setStroke(strokeBefore);
          //Stroke is how java fills objects. oracle docs gives info
          break;
        case "RightFlipper":
          g2.setPaint(gizmo.getColor());

          RightFlipper rightFlipper = (RightFlipper) gizmo;
          Stroke strokeBeforeRight = g2.getStroke();
          g2.setStroke(new BasicStroke((float) 12.5, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));
          g2.draw(new Line2D.Double(rightFlipper.getBoardXDoouble()[0] * PIXELS_PER_GRID_BOX, rightFlipper.getBoardYDouble()[0] * PIXELS_PER_GRID_BOX, rightFlipper.getBoardXDoouble()[1] * PIXELS_PER_GRID_BOX, rightFlipper.getBoardYDouble()[1] * PIXELS_PER_GRID_BOX));


          g2.setStroke(strokeBeforeRight);
          break;
        default:
          System.out.println("Some gizmo");
          break;
      }
      repaint();

    }
  for(Ball b : model.getBalls()) {
    if (b != null) {
      g2.setColor(b.getColor());
      double x = (b.getX() - b.getRadius()) * PIXELS_PER_GRID_BOX;
      double y = (b.getY() - b.getRadius()) * PIXELS_PER_GRID_BOX;
      double width = (2 * b.getRadius()) * PIXELS_PER_GRID_BOX;
      Ellipse2D.Double shape = new Ellipse2D.Double(x, y, width, width);
      g2.fill(shape);
//      if (ballSpeed != null)
     // ballSpeed.setText("Ball Speed: " + b.getVelocity().length());
    }
  }
  }

  public void setModel(fakeModel model) {
    this.model = model;
  }

  @Override
  public void update(Observable observable, Object o) {
    repaint();
    this.requestFocus();
  }
}
