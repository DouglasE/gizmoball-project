import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;

public class GizmoView {
    private static JFrame window;
    private static JPanel runPanel;
    private static JPanel buildPanel;

    private Board board;
    private static boolean isrunning = false;
    public GizmoView(fakeModel model,Timer t) {
        //Use model to pass gizmos to the Board and that will paint it
        //This is used to inform them of the state that they're in.
        JLabel infoLabel = new JLabel("WELCOME TO GIZMOBALL");
        JPanel infoPanel = new JPanel();
        window = new JFrame("Gizmoball");
        Board board = new Board(model);
        this.board = board;
        board.requestFocus();
        BoardModifyListener boardModifier = new BoardModifyListener(model,infoLabel);
        board.addMouseListener(boardModifier);
        board.addMouseMotionListener(boardModifier);

        runPanel = new RunPanel(model, this,infoLabel,t);

        buildPanel = new BuildPanel(model,boardModifier, this,infoLabel);
        buildPanel.setBackground(Color.LIGHT_GRAY);

        Action doNothing = new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                model.stopAllBalls();
            }
        };

        board.getInputMap().put(KeyStroke.getKeyStroke("T"),"pressed");
        board.getActionMap().put("pressed",doNothing);
        //window.setLayout(new GridLayout(0, 2));
        window.setLayout(new BorderLayout());
        window.getContentPane().add(buildPanel, BorderLayout.WEST);
        window.getContentPane().add(infoPanel,BorderLayout.NORTH);
        infoPanel.add(infoLabel,BorderLayout.CENTER);
        window.getContentPane().add(board, BorderLayout.EAST);


        window.pack();
        window.setVisible(true);
        window.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        window.setResizable(false);
    }


    public static void setToRun(){
        buildPanel.setVisible(false);
        runPanel.setVisible(true);
        window.remove(buildPanel);
        window.add(runPanel);
        isrunning = true;
    }

    public static void setToBuild() {
        buildPanel.setVisible(true);
        runPanel.setVisible(false);
        window.remove(runPanel);
        window.add(buildPanel);
        isrunning = false;
    }

    public static boolean isrunning(){
        return isrunning;
    }

    public Board getBoard() {
        return board;
    }

    public void updateSliders() {
        BuildPanel panel = (BuildPanel) buildPanel;
        panel.updateSliders();
    }
}
