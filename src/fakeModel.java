import Exceptions.DuplicateGizmoException;
import Exceptions.badSelectException;
import Exceptions.noSuchConnectionException;
import physics.Circle;
import physics.Geometry;
import physics.LineSegment;
import physics.Vect;

import javax.sound.sampled.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.List;

public class fakeModel extends Observable {

    public final HashMap<String, Gizmo> gizmoHashMap;

    //Key is producer, Value is consumer.
    private final Queue<String> keyPresses;
    private  double gravity;
    private Walls gizmo_walls;
    private static double mu1 = 0.025D;
    private static double mu2 = 0.025D;
    private List<Ball> balls;
    private boolean deleteMode, selectMode,
            moveMode, rotateMode, disconnectMode,
            connectMode, addBallMode, addGizmoMode, keyConnectMode, keyDisconnectMode = false;
    private boolean acceptingKeypresses,changeColorMode,playAudio,querymode = false;
    private File currentFile;

    public fakeModel() {
        balls = new ArrayList<>();
        keyPresses = new ArrayDeque<>();
        gizmoHashMap = new HashMap<>();
        playAudio = true;

        gravity = 25.0D;
        gizmo_walls = new Walls(0, 0, 20, 20);


    }

    public void addGizmo(Gizmo gizmoToAdd, String uniqueIdentifier) throws DuplicateGizmoException, badSelectException {
        if(gizmoToAdd.getX()  < 0  || gizmoToAdd.getX() > 19 || gizmoToAdd.getY()  < 0 || gizmoToAdd.getX() > 19){
            throw new badSelectException("Bad placement, invalid co-ordinate");
        }
        if (gizmoHashMap.containsKey(uniqueIdentifier)) {
            throw new DuplicateGizmoException("Bad gizmo ID!");
        }
        gizmoHashMap.put(uniqueIdentifier,gizmoToAdd);




    }

    public void rotateGizmo(String uniqueIdentifier) {
      if(!gizmoHashMap.containsKey(uniqueIdentifier)){
        return;
      }
        gizmoHashMap.get(uniqueIdentifier).rotate();
    }

    public void removeGizmo(String gizmoID) throws noSuchConnectionException {

        for(Ball b : balls){
          if (b.getID().equalsIgnoreCase(gizmoID)){
              balls.remove(b);
              updateObservers();
              return;
          }
        }

        for(Gizmo g : getGizmos()){
            if(g.getGizmoConnections().contains(gizmoID)){
                g.removeConnection(gizmoID);
            }
        }
      gizmoHashMap.remove(gizmoID);
    }

    public boolean addKeyConnect(String gizmoID, String keyNum, String upordown) {
        Gizmo gizmo = gizmoHashMap.get(gizmoID);
        return gizmo.addKeyConnect(keyNum, upordown);
    }

    public void addKeyPress(String key) {

        if (keyPresses.size() <= 6) {
            if (keyPresses.isEmpty()) {
                keyPresses.add(key);
            } else if (!keyPresses.contains(key)) {
                keyPresses.add(key);
            }
        } else {
            keyPresses.poll();
        }
    }

    public Queue<String> getKeyPresses(){
        return new ArrayDeque<>(keyPresses);
    }
    public void addBall(String ballID, float xCoord, float yCoord, float xVel, float yVel) {

        Ball balltoAdd = new Ball(ballID, xCoord, yCoord, xVel, yVel);
        balls.add(balltoAdd);
        this.setChanged();
        this.notifyObservers();
    }
    public void toggleAudio(){
        playAudio = !playAudio;
    }
    public boolean isAudioOn(){return playAudio;}
    public void removeBalls( ){
        for(Ball b : balls){
            b.setX(-1);
            b.setY(-1);
        }
    }

    private void applyGravity(Ball ball,double time) {
        double changeAmount = gravity * time;
        Vect change = new Vect(0,changeAmount);
        ball.setVector(ball.getVelocity().plus(change));
    }

    private void applyFriction(Ball ball, double time) {
        //Vnew = Vold * (1 - mu * delta_t - mu2 * |Vold| * delta_t).
        double change = 1 - mu1 * time - mu2 * ball.getVelocity().length() * time;
        ball.setVector(ball.getVelocity().times(change));

    }

    public List<Ball> getBalls(){
        return balls;
    }
    public void stopAllBalls(){
        for(Ball b : balls) {
          if (!b.isStopped())
            b.stop();
        }
        }

    public boolean allBallsStopped() {
boolean allstopped = true;
    for(Ball b : balls){
        if(!b.isStopped()){
            allstopped = false;
        }
    }
    return allstopped;
    }
    public void moveBalls() {

        double moveTime = 0.01D;
        //Check keypresses
        for (Gizmo gizmo : getGizmos()) {
            for (String key : keyPresses) {
                if (!gizmo.getKeyConnections().contains(key)) {
                    continue;
                }
                if (acceptingKeypresses && !allBallsStopped()) {
                    if (gizmo instanceof LeftFlipper) {
                        ((LeftFlipper) gizmo).stateSwap();
                    } else if (gizmo instanceof RightFlipper) {
                        ((RightFlipper) gizmo).stateSwap();
                    } else {
                        gizmo.trigger();
                    }
                }
            }
        }


        for (Ball b : balls) {
            if (b != null && !b.isStopped() && b.isActive()) {
                CollisionDetails details = timeUntilCollision(b);
                double time_until_collision = details.getTuc();
                //Some if statement about timeUntilCollision being smaller or greater than the moveTime, so you move the b for the shorter amount of time
                //If there's been a collision (see above comment), then set the velocity of the b to the new velocity object obtained from the collisionDetails object
                if (time_until_collision > moveTime) {
                    // No collision ...
                    moveBallForTime(b,moveTime);
                    b.resetCollisionCount();
                } else {
                    moveBallForTime(b,time_until_collision);
                    // Post collision velocity ...
                    b.setVector(details.getVector());
                    Gizmo gizmo = details.getGizmoHit();
                    if (b.getLastHit() == gizmo) {
                        b.incrementCollisioncount();
                    } else {
                        b.resetCollisionCount();
                    }
                    if (gizmo == null){
                        b.setVector(details.getPair().v2);
                        Ball otherBall = balls.get(balls.indexOf(details.getBallHit()));
                        otherBall.setVector(details.getPair().v1);
                            if(otherBall.isStopped()){
                                otherBall.start();;
                            }
                    }

                    if (!b.isStopped() && gizmo != null) {

                            gizmo.trigger();

                    }
                    if (gizmo != null) {
                        if(playAudio) {
                            playGizmoAudio(details.getGizmoHit());
                        }
                        Triggering(b, details, gizmo);
                        b.setLastHit(gizmo);


                    }
                    if(b.getCollisionCount() > 20){
                        b.stop();
                    }

                }
              applyFriction(b,moveTime);
              applyGravity(b,moveTime);
                this.setChanged();
                this.notifyObservers();

            }

        }

        keyPresses.poll();
    }

    public void resetBalls(){
        for(Ball b :getBalls()){
            b.reset();
        }
    }

    private void Triggering(Ball b, CollisionDetails details, Gizmo gizmo) {

        if (details.getGizmoHit().getType().equalsIgnoreCase("absorber")) {
            Absorber absorber = (Absorber) details.getGizmoHit();
            absorber.addBall(b);
        } else if (details.getGizmoHit().getType().equalsIgnoreCase("teleporter")) {
            int y = gizmo.getY();
            int x = gizmo.getX();
            b.setX(x + 0.5);
            b.setY(y + 1.25 + b.getRadius());

            b.setVector(new Vect(b.getVelocity().x(), Math.abs(b.getVelocity().y())));

        } else {
            for (String Connection : gizmo_walls.getGizmoConnections()) {
                if (gizmoHashMap.get(Connection) != null) {
                    gizmoHashMap.get(Connection).trigger();
                }
            }

        }
        for (String Connection : gizmo.getGizmoConnections()) {
            if (gizmoHashMap.get(Connection) != null) {
                if (gizmoHashMap.get(Connection) instanceof LeftFlipper) {
                    ((LeftFlipper) gizmoHashMap.get(Connection)).stateSwap();
                } else if (gizmoHashMap.get(Connection) instanceof RightFlipper) {
                    ((RightFlipper) gizmoHashMap.get(Connection)).stateSwap();
                } else {
                    gizmoHashMap.get(Connection).trigger();
                }

            }
        }

        if (gizmo instanceof LeftFlipper) {
            ((LeftFlipper) gizmo).stateSwap();
        } else if (gizmo instanceof RightFlipper) {
            ((RightFlipper) gizmo).stateSwap();
        }
    }

    private void playGizmoAudio(Gizmo gizmo) {


        StringBuilder filename = new StringBuilder("audio/");
        switch (gizmo.getType()){
            case "Square":
                filename.append("basic.wav");
                break;
            case "Triangle":
                filename.append("basic.wav");
                break;
            case "Circle":
                filename.append("basic.wav");
                break;
            case "Absorber":
                filename.append("chomp.wav");
                break;
            case "Teleporter":
                filename.append("suck.wav");
                break;
            default:
                filename.append("basic.wav");
        }
        try {
            AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(new File(filename.toString()).getAbsoluteFile());
            Clip clip = AudioSystem.getClip();
            clip.open(audioInputStream);
            clip.start();

        } catch (UnsupportedAudioFileException | IOException | LineUnavailableException e) {
            e.printStackTrace();
        }
    }

    private void moveBallForTime(Ball b, double time) {
        //  System.out.println("Ball starting velocity (length) = " + theBall.getVelocity().length());

        double newX = 0.0D,
                newY = 0.0D;
        double xVelo = b.getVelocity().x();
        double yVelo = b.getVelocity().y();


        newX = b.getX() + (xVelo * time);
        newY = b.getY() + (yVelo * time);
        // System.out.println("Ball moved to " + newX + ", " + newY);

        b.setX(newX);
        b.setY(newY);

        //  System.out.println("Ball ending velocity (length) = " + theBall.getVelocity().length());
    }


    //Boolean because we need to see if the space is already occupied.
    public void toggleKeys(boolean flag) {
        acceptingKeypresses = flag;
    }

    public void moveGizmo(String gizmoID, double firstNumber, double secondNumber) throws badSelectException {
      for(Ball b : balls){
        if (b.getID().equalsIgnoreCase(gizmoID)){
          b.setX(firstNumber);
          b.setY(secondNumber);
          return;
        }
      }
        gizmoHashMap.get(gizmoID).move((int)firstNumber, (int)secondNumber);

    }


    public void addConnection(String Producer, String Consumer) throws badSelectException, noSuchConnectionException {
        if(Producer.equalsIgnoreCase("outerwalls")){
            gizmo_walls.addGizmoConnection(Consumer);
            return;
        }
        Gizmo gizmo = gizmoHashMap.get(Producer);

        if (gizmo != null && !Consumer.isEmpty()) {
            //TODO: Should really return an error or exception if this is null
          if(gizmo.getGizmoConnections().contains(Consumer)){
            throw new noSuchConnectionException("Connection already exists!");
          }
            gizmo.addGizmoConnection(Consumer);
        }
        else{

          throw new  badSelectException("Empty space selected");
        }

    }
    public void removeKeyConnect(String ID){
        gizmoHashMap.get(ID).removeKeyConnects();
    }
    public void removeConnection(String RemoveFrom, String toBeRemoved) throws noSuchConnectionException {
        gizmoHashMap.get(RemoveFrom).removeConnection(toBeRemoved);

    }

    public Walls getWalls(){
        return gizmo_walls;
    }
    private CollisionDetails timeUntilCollision(Ball ball) {
        Circle ballCircle = ball.getCircle();
        Vect ballVelocity = ball.getVelocity();
        Vect newVelo = new Vect(0, 0);

        double shortestTime = Double.MAX_VALUE;
        double time = 0.0;
        ArrayList<LineSegment> lss = gizmo_walls.getLineSegments();
        Gizmo collision = null;
        for (LineSegment line : gizmo_walls.getLineSegments()) {
            time = Geometry.timeUntilWallCollision(line, ballCircle, ballVelocity);
            if (time < shortestTime) {
                shortestTime = time;
                newVelo = Geometry.reflectWall(line, ballVelocity, 1.0);

                collision = new Square("outerwalls",-1,-1);
            }
        }

        for (Gizmo gizmo : getGizmos()) {
            if (gizmo.getLineSegments() != null) {
                for (LineSegment line : gizmo.getLineSegments()) {
                    time = Geometry.timeUntilWallCollision(line, ballCircle, ballVelocity);
                    if (time < shortestTime) {
                        collision = gizmo;
                        shortestTime = time;
                        newVelo = Geometry.reflectWall(line, ballVelocity, 1);
                    }

                    if (gizmo instanceof LeftFlipper) {

                        LeftFlipper flipper = (LeftFlipper) gizmo;
                        double angVel = 0;
                        if(flipper.currentlyRotating()){
                            angVel = Math.toRadians(1080);
                        }
                        Vect rotationpoint = flipper.getCenterOfRotation();
                        time = Geometry.timeUntilRotatingWallCollision(line, rotationpoint, -angVel, ballCircle, ballVelocity);
                        if (time < shortestTime) {
                            shortestTime = time;
                            newVelo = Geometry.reflectRotatingWall(line, rotationpoint, -angVel, ballCircle, ballVelocity, 0.95);
                            collision = gizmo;
                        }

                    }

                    //Sorry for this duplicate code ;(
                    if (gizmo instanceof RightFlipper) {

                        RightFlipper flipper = (RightFlipper) gizmo;
                        double angVel = 0;
                        if(flipper.currentlyRotating()){
                            angVel = Math.toRadians(1080);
                        }
                        Vect rotationpoint = flipper.getCenterOfRotation();
                        time = Geometry.timeUntilRotatingWallCollision(line, rotationpoint, angVel, ballCircle, ballVelocity);
                        if (time < shortestTime) {
                            shortestTime = time;
                            newVelo = Geometry.reflectRotatingWall(line, rotationpoint, angVel, ballCircle, ballVelocity, 0.95);
                            collision = gizmo;
                        }

                    }

                }
            }
            //TODO: Remove this null check when all gizmos have their getCircles implemented,

                for (Circle circles : gizmo.getCircles()) {
                    time = Geometry.timeUntilCircleCollision(circles, ballCircle, ballVelocity);
                    if (time < shortestTime) {
                        collision = gizmo;
                        shortestTime = time;
                        newVelo = Geometry.reflectCircle(circles.getCenter(), ball.getCircle().getCenter(), ballVelocity);
                    }
                    if (gizmo instanceof LeftFlipper) {

                        LeftFlipper flipper = (LeftFlipper) gizmo;
                        double angVel = 0;
                        if(flipper.currentlyRotating()){
                            angVel = Math.toRadians(1080);
                        }
                        Vect rotationpoint = flipper.getCenterOfRotation();
                        time = Geometry.timeUntilRotatingCircleCollision(circles, rotationpoint, -angVel, ballCircle, ballVelocity);
                        if (time < shortestTime) {
                            shortestTime = time;
                            newVelo = Geometry.reflectRotatingCircle(circles, rotationpoint, -angVel, ballCircle, ballVelocity, 0.95);
                            collision = gizmo;
                        }

                    }
                    if (gizmo instanceof RightFlipper) {

                        RightFlipper flipper = (RightFlipper) gizmo;
                        double angVel = 0;
                        if(flipper.currentlyRotating()){
                            angVel = Math.toRadians(1080);
                        }
                        Vect rotationpoint = flipper.getCenterOfRotation();
                        time = Geometry.timeUntilRotatingCircleCollision(circles, rotationpoint, angVel, ballCircle, ballVelocity);
                        if (time < shortestTime) {
                            shortestTime = time;
                            newVelo = Geometry.reflectRotatingCircle(circles, rotationpoint, angVel, ballCircle, ballVelocity, 0.95);
                            collision = gizmo;
                        }

                    }


                }



        }

        CollisionExtension c = null;
        Geometry.VectPair pair = null;
        Ball ballcollision = null;
        for(Ball b : getBalls()){
            if(b.getID().equalsIgnoreCase(ball.getID())){
                continue;
            }
            else{
                time = Geometry.timeUntilBallBallCollision(b.getCircle(),b.getVelocity(),ball.getCircle(),ball.getVelocity());
                if (time < shortestTime) {
                    //TODO make this non null!!!
                    shortestTime = time;
                    pair = Geometry.reflectBalls(b.getCircle().getCenter(), 1, b.getVelocity(), ball.getCircle().getCenter(), 1,ball.getVelocity());
                    ballcollision = b;
                }
            }
        }
        if(pair != null){
            return new CollisionDetails(shortestTime,pair,ballcollision);
        }else {
            return new CollisionDetails(shortestTime, newVelo, collision);
        }

    }

    public void clear() {
        //circles.clear();
        //triangles.clear();
        //leftFlipperList.clear();
        //RightflipperList.clear();
        gizmoHashMap.clear();
        balls.clear();
        gizmo_walls.clearConnections();
        removeBalls();
        updateObservers();
    }

    public void updateObservers() {
        setChanged();
        notifyObservers();
    }
    public List<Gizmo> getGizmos() {
        return new ArrayList<>(gizmoHashMap.values());
    }
    public String getGizmoAt(double x, double y) throws badSelectException {

        for(Ball b : balls){
            Vect center = new Vect(b.getX()-0.25,b.getY()-0.25);
            Vect bound  = b.getCircle().getCenter().plus(new Vect(0.25,0.25));
            
            if(center.x() < x &&  bound.x() >= x && center.y()<y && bound.y() >= y){

                    return b.getID();
                }
        }


        for (Gizmo g : getGizmos()) {

            if(g instanceof Absorber){
                Absorber abs = (Absorber) g;

                if((int)x >= abs.getX() && (int)x < abs.getX2Coord() && (int)y >= abs.getY() && (int)y < abs.getY2Coord() || (int)x==abs.getX2Coord()-1 && (int)y==abs.getY()-1){
                    return abs.getID();
                }
            }

            if (g instanceof LeftFlipper || g instanceof RightFlipper) {
                if (x >= g.getX() && x <g.getX()+2 && y>= g.getY() && y<g.getY()+2) {
                    return g.getID();
                }
            }

            if (g instanceof TelePorter) {
                if (x >= g.getX() && x<g.getX()+1 && y>=g.getY() && y<g.getY()+2) {
                    return g.getID();
                }
            }

            if (g.getX() == (int) x && g.getY() == (int) y) {
                return g.getID();
            }


        }



        return "";
//        throw new badSelectException("No gizmo at location!");


    }

    public int generateGizmoID(String shape) {
        int gizmoID = 0;
        for (Gizmo gizmo : getGizmos()) {
            if (gizmo.getType().equalsIgnoreCase(shape)) {
                String idNumbers = gizmo.getID().replaceAll("[^0-9]", "");
                idNumbers = idNumbers.trim();

                if (idNumbers.equals("")) {
                    idNumbers = "0";
                }

                if (Integer.parseInt(idNumbers) >= gizmoID) {
                    gizmoID = Integer.parseInt(idNumbers) + 1;
                }
            }
        }
        if (shape.equalsIgnoreCase("ball")) {
            for (Ball b : getBalls()) {
                String idNumbers = b.getID().replaceAll("[^0-9]", "");
                idNumbers = idNumbers.trim();

                if (idNumbers.equals("")) {
                    idNumbers = "0";
                }

                if (Integer.parseInt(idNumbers) >= gizmoID) {
                    gizmoID = Integer.parseInt(idNumbers) + 1;
                }

            }
        }
        return gizmoID;
    }
    public Gizmo getGizmo(String ID){
        return gizmoHashMap.get(ID);
    }








    public boolean getRotateMode() {
        return rotateMode;
    }

    public boolean getSelectMode() {
        return selectMode;
    }


    public boolean getMoveMode() {
        return moveMode;
    }

    public boolean getConnectMode() {
        return connectMode;
    }

    public boolean getDisconnectMode() {
        return disconnectMode;
    }

    public boolean getDeleteMode() {
        return deleteMode;
    }

    public boolean getAddBallMode() {
        return addBallMode;
    }

    public boolean getAddGizmoMode() {
        return addGizmoMode;
    }

    public void setRotateMode(boolean bool) {
        setAllModesFalse();
        rotateMode = bool;
    }

    public void setSelectedMode(boolean bool) {
        setAllModesFalse();
        selectMode = bool;
    }


    public void setAllModesFalse() {
        selectMode = false;
        deleteMode = false;
        rotateMode = false;
        connectMode = false;
        disconnectMode = false;
        addBallMode = false;
        addGizmoMode = false;
        moveMode = false;
        keyDisconnectMode = false;
        keyConnectMode = false;
        changeColorMode = false;
    }
    public void setMoveMode(boolean bool) {
        setAllModesFalse();
        this.moveMode = bool;
    }

    public void setConnectMode(boolean bool) {
        setAllModesFalse();
        connectMode = bool;
    }

    public void setDisconnectMode(boolean bool) {
        setAllModesFalse();
        disconnectMode = bool;
    }

    public void setDeleteMode(boolean bool) {
        setAllModesFalse();
        deleteMode = bool;
    }
  public void setChangeColorMode(boolean bool) {
    setAllModesFalse();
    changeColorMode= bool;
  }

    public void setAddBallMode(boolean bool) {
        setAllModesFalse();
        addBallMode = bool;
    }
    public void setAddGizmoMode(boolean bool) {
        setAllModesFalse();
        addGizmoMode = bool;
    }
    public void setQuerymode(boolean bool){
        setAllModesFalse();
        querymode =true;
    }
    public boolean getQueryMode(){
        return querymode;
    }

    public void setKeyConnectMode(boolean bool) {
        setAllModesFalse();
        keyConnectMode = bool;
    }

    public void setKeyDisconnectMode(boolean bool) {
        setAllModesFalse();
        keyDisconnectMode = bool;
    }
    public boolean isAcceptingKeypresses() {
        return this.acceptingKeypresses;
    }

    public void purgeKeyPresses() {
        keyPresses.clear();
    }


    public boolean getKeyDisconnectMode() {
        return keyDisconnectMode;
    }


    public boolean getKeyConnectMode() {
        return keyConnectMode;
    }
    public boolean getChangeColorMode(){ return changeColorMode;}
    public String getFrictionOverTime() {
        return Double.toString(mu1);
    }

    public String getFrictionOverDistance() {
        return Double.toString(mu2);
    }
    public void setFrictionOverTime(double newFriction){
        if(newFriction > 0) {
            mu1 = newFriction;
        }
    }

    //TODO THROW EXCEPTION IF THIS IS LESS THAN 0!
    public void setFrictionOverDistance(double newFriction){

        if(newFriction > 0) {
            mu2 = newFriction;
        }
    }
    public void setGravity(double newGravity){
        if(newGravity > 0){
            gravity = newGravity;
        }

    }
    public String getGravity(){
        return  Double.toString(gravity);
    }

    public void startAllBalls() {
        for(Ball b : balls){
          if(b.isStopped()) {
            b.start();
          }
        }
    }

    public void reset() {
        for(Ball b :getBalls()){
            b.reset();
        }
        for(Gizmo g : getGizmos()){
            g.reset();
        }
    }

  public void setGizmoColour(String id, Color color) {
      for(Ball b : balls){
        if(b.getID().equalsIgnoreCase(id)){
          b.setColor(color);
            return;
        }
      }
      gizmoHashMap.get(id).setColor(color);
  }

  public boolean hasGizmo(String id) {
        for (Gizmo gizmo: getGizmos()) {
            if (gizmo.getID().equalsIgnoreCase(id)) {
                return true;
            }
        }
        return false;
  }

  public void resetFlippers() {
        for (Gizmo gizmo: getGizmos()) {
            if (gizmo instanceof LeftFlipper) {
                LeftFlipper flipper = (LeftFlipper) gizmo;
                flipper.resetToBottom();
                continue;
            }
            if (gizmo instanceof RightFlipper) {
                RightFlipper flipper = (RightFlipper) gizmo;
                flipper.resetToBottom();
                continue;
            }
        }
  }

    public File getFile() {
        return currentFile;
    }

    public void setFile(File file) {
        this.currentFile = file;
    }
}
