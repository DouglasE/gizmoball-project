import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class BuildModeSwitchListener implements ActionListener {
    private fakeModel model;
    private JButton startButton;
    private JLabel infoLabel;
    public BuildModeSwitchListener(fakeModel model, JButton startButton,JLabel infoLabel) {
        this.model = model;
        this.startButton = startButton;
        this.infoLabel = infoLabel;
    }
    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        GizmoView.setToBuild();
        model.resetFlippers();
        infoLabel.setText("Ready to build!");
        infoLabel.setBackground(Color.green);
        infoLabel.setBorder(BorderFactory.createBevelBorder(1));
        model.stopAllBalls();
        model.toggleKeys(false);
        startButton.setText("Start");
    }
}
