import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class FrictionTimeSliderListener implements ChangeListener {
    private fakeModel model;
    public FrictionTimeSliderListener(fakeModel model){
        this.model = model;
    }
    @Override
    public void stateChanged(ChangeEvent changeEvent) {
        //We divide these by 1000 in the listener to get the correct value

        JSlider source = (JSlider)changeEvent.getSource();
        double newFriction = (double)source.getValue() / 1000;
        model.setFrictionOverTime(newFriction);
    }
}
