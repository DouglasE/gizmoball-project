import static org.junit.Assert.*;

import Exceptions.DuplicateGizmoException;
import Exceptions.badSelectException;
import Exceptions.noSuchConnectionException;

import org.junit.Before;
import org.junit.Test;
import javax.swing.event.MouseInputListener;
import java.awt.*;
import java.rmi.MarshalledObject;
import java.util.ArrayList;
import java.util.Arrays;


public class gizmoTests {


  private fakeModel model;

  @Before
  public void initialiseGizmo() {
    model = new fakeModel();
    //Pointless thing for coverage :imp_emoji:
    model.getWalls();
  }

  @Test
  public void basicTests() {
    assertEquals(model.getFrictionOverTime(), "0.025");
    assertEquals(model.getFrictionOverDistance(), "0.025");
    model.setFrictionOverDistance(1.0);
    model.setFrictionOverTime(1.0);
    assertEquals(model.getFrictionOverDistance(), Double.toString(1.0));
    assertEquals(model.getFrictionOverTime(), Double.toString(1.0));

    model.setGravity(15);
    assertEquals(model.getGravity(), "15.0");
  }

  @Test
  public void simpleSquareAdd() throws DuplicateGizmoException, badSelectException {
    Gizmo simpleSquare = new Square("1", 1, 1);
    model.addGizmo(simpleSquare, "1");
    assertEquals(model.getGizmoAt(1, 1), simpleSquare.getID());
    assertTrue(model.getGizmos().contains(simpleSquare));

  }

  @Test
  public void simpleCircleAdd() throws DuplicateGizmoException, badSelectException {
    Gizmo simpleCircle = new CircleGizmo("2", 1, 1);
    model.addGizmo(simpleCircle, "1");
    assertEquals(model.getGizmoAt(1, 1), simpleCircle.getID());
    assertTrue(model.getGizmos().contains(simpleCircle));
  }

  @Test
  public void simpleTriangleAdd() throws DuplicateGizmoException, badSelectException {
    Gizmo simpleTriangle = new Triangle("2", 1, 1);
    model.addGizmo(simpleTriangle, "1");
    assertEquals(model.getGizmoAt(1, 1), simpleTriangle.getID());
    assertTrue(model.getGizmos().contains(simpleTriangle));
  }

  @Test
  public void simpleRightFlipperAdd() throws DuplicateGizmoException, badSelectException {
    Gizmo simpleRight = new RightFlipper("2", 1, 1);
    model.addGizmo(simpleRight, "1");
    assertEquals(model.getGizmoAt(1, 1), simpleRight.getID());
    assertTrue(model.getGizmos().contains(simpleRight));
  }

  @Test
  public void simpleLeftFlipperAdd() throws DuplicateGizmoException, badSelectException {
    Gizmo simpleLeft = new LeftFlipper("2", 1, 1);
    model.addGizmo(simpleLeft, "1");
    assertEquals(model.getGizmoAt(1, 1), simpleLeft.getID());
    assertTrue(model.getGizmos().contains(simpleLeft));
  }

  @Test(expected = DuplicateGizmoException.class)
  public void simpleBadGizmoTest() throws badSelectException, DuplicateGizmoException {
    Gizmo simpleCircle = new CircleGizmo("2", 1, 1);
    Gizmo simpleTriangle = new Triangle("2", 1, 1);
    model.addGizmo(simpleCircle, simpleCircle.getID());
    model.addGizmo(simpleTriangle, simpleTriangle.getID());
  }

  @Test(expected = badSelectException.class)
  public void simpleBadPlacementTest() throws badSelectException, DuplicateGizmoException {
    Gizmo simpleSquare = new Square("1", -1, 21);
    model.addGizmo(simpleSquare, simpleSquare.getID());
  }

  @Test
  public void testRotate() throws badSelectException, DuplicateGizmoException {
    Gizmo simpleSquare = new Square("1", 12, 13);
    model.addGizmo(simpleSquare, simpleSquare.getID());
    model.rotateGizmo(simpleSquare.getID());
    assertSame(simpleSquare.getRotation(), ROTATION.NONE);


    Gizmo simpleCircle = new CircleGizmo("2", 1, 1);
    model.addGizmo(simpleCircle, simpleCircle.getID());

    model.rotateGizmo(simpleCircle.getID());
    assertSame(simpleCircle.getRotation(), ROTATION.NONE);


    Gizmo simpleTriangle = new Triangle("3", 15, 15);
    model.addGizmo(simpleTriangle, simpleTriangle.getID());
    model.rotateGizmo(simpleTriangle.getID());
    assertSame(simpleTriangle.getRotation(), ROTATION.BOTTOMRIGHT);

    model.rotateGizmo(simpleTriangle.getID());
    assertSame(simpleTriangle.getRotation(), ROTATION.BOTTOMLEFT);

    model.rotateGizmo(simpleTriangle.getID());
    assertSame(simpleTriangle.getRotation(), ROTATION.TOPRIGHT);

    model.rotateGizmo(simpleTriangle.getID());
    assertSame(simpleTriangle.getRotation(), ROTATION.TOPLEFT);

    Gizmo LeftFlipper = new LeftFlipper("4", 15, 15);
    model.addGizmo(LeftFlipper, LeftFlipper.getID());
    model.rotateGizmo(LeftFlipper.getID());
    assertSame(LeftFlipper.getRotation(), ROTATION.TOPRIGHT);

    model.rotateGizmo(LeftFlipper.getID());
    assertSame(LeftFlipper.getRotation(), ROTATION.BOTTOMRIGHT);

    model.rotateGizmo(LeftFlipper.getID());
    assertSame(LeftFlipper.getRotation(), ROTATION.BOTTOMLEFT);

    model.rotateGizmo(LeftFlipper.getID());
    assertSame(LeftFlipper.getRotation(), ROTATION.TOPLEFT);



        Gizmo RightFlipper = new RightFlipper("5",15,15);
        model.addGizmo(RightFlipper,RightFlipper.getID());
        model.rotateGizmo(RightFlipper.getID());
        assertSame(RightFlipper.getRotation(), ROTATION.TOPRIGHT);

        model.rotateGizmo(RightFlipper.getID());
        assertSame(RightFlipper.getRotation(),ROTATION.BOTTOMRIGHT);

        model.rotateGizmo(RightFlipper.getID());
        assertSame(RightFlipper.getRotation(),ROTATION.BOTTOMLEFT);

        model.rotateGizmo(RightFlipper.getID());
        assertSame(RightFlipper.getRotation(),ROTATION.TOPLEFT);

    Gizmo Absorber = new Absorber("6", 15, 15, 16, 16);
    model.addGizmo(Absorber, Absorber.getID());
    model.rotateGizmo(Absorber.getID());
    assertSame(Absorber.getRotation(), ROTATION.NONE);

  }

  @Test
  public void removetest() throws badSelectException, DuplicateGizmoException, noSuchConnectionException {

    Gizmo simpleSquare = new Square("1", 12, 13);
    model.addGizmo(simpleSquare, simpleSquare.getID());
    model.removeGizmo(simpleSquare.getID());
    assertTrue(!model.getGizmos().contains(simpleSquare));


    Gizmo simpleCircle = new CircleGizmo("2", 1, 1);
    model.addGizmo(simpleSquare, simpleSquare.getID());
    model.addGizmo(simpleCircle, simpleCircle.getID());
    model.addConnection(simpleCircle.getID(), simpleSquare.getID());
    model.removeGizmo(simpleSquare.getID());
    model.removeGizmo(simpleCircle.getID());

  }

  @Test
  public void keyTest() throws badSelectException, DuplicateGizmoException {
    Gizmo simpleSquare = new Square("1", 12, 13);
    model.addGizmo(simpleSquare, simpleSquare.getID());
    model.addKeyConnect(simpleSquare.getID(), "32", "down");
    assertTrue(simpleSquare.getKeyConnections().contains("key.32.down"));

    Gizmo simpleCircle = new CircleGizmo("2", 4, 4);
    model.addGizmo(simpleCircle, simpleCircle.getID());
    model.addKeyConnect(simpleCircle.getID(), "32", "down");
    assertTrue(simpleCircle.getKeyConnections().contains("key.32.down"));


    Gizmo simpleTriangle = new Triangle("3", 6, 6);
    model.addGizmo(simpleTriangle, simpleTriangle.getID());
    model.addKeyConnect(simpleTriangle.getID(), "32", "down");
    assertTrue(simpleTriangle.getKeyConnections().contains("key.32.down"));

    Gizmo LeftFlipper = new LeftFlipper("4", 16, 12);
    model.addGizmo(LeftFlipper, LeftFlipper.getID());
    model.addKeyConnect(LeftFlipper.getID(), "32", "down");
    assertTrue(LeftFlipper.getKeyConnections().contains("key.32.down"));

    Gizmo RightFlipper = new RightFlipper("5", 11, 3);
    model.addGizmo(RightFlipper, RightFlipper.getID());
    model.addKeyConnect(RightFlipper.getID(), "32", "down");
    assertTrue(RightFlipper.getKeyConnections().contains("key.32.down"));

    model.removeKeyConnect(RightFlipper.getID());
    assertTrue(RightFlipper.getKeyConnections().isEmpty());
  }

  @Test
  public void keyPressTest() {
    model.addKeyPress("32");
    assertTrue(model.getKeyPresses().contains("32"));
    model.addKeyPress("34");
    model.addKeyPress("35");
    model.addKeyPress("36");
    model.addKeyPress("37");
    model.addKeyPress("38");
    model.addKeyPress("39");
    model.addKeyPress("40");
    assertEquals(6, model.getKeyPresses().size());
  }


  @Test
  public void BallTest() {
    model.addBall("BALLONE", 12, 12, 0, 0);
    assertTrue(model.getBalls().contains(new Ball("BALLONE", 12, 12, 0, 0)));
    model.removeBalls();
    assertEquals(model.getBalls().get(0).getX(), -1, 0.5);
    assertEquals(model.getBalls().get(0).getY(), -1, 0.5);
  }

  @Test
  public void moveBallsTest() throws badSelectException, DuplicateGizmoException {
    model.addBall("BALLONE", 12, 12, 0, 0);
    Gizmo simpleSquare = new Square("1", 12, 13);
    model.toggleKeys(true);
    assertTrue(model.isAcceptingKeypresses());
    assertTrue(model.getKeyPresses().isEmpty());
    Gizmo LeftFlipper = new LeftFlipper("4", 16, 12);

    model.addGizmo(LeftFlipper, LeftFlipper.getID());
    model.addKeyConnect(LeftFlipper.getID(), "32", "down");

    Gizmo RightFlipper = new RightFlipper("5", 11, 3);
    model.addGizmo(RightFlipper, RightFlipper.getID());

    model.addGizmo(simpleSquare, simpleSquare.getID());
    model.addKeyConnect(simpleSquare.getID(), "32", "down");

    model.getBalls().get(0).start();
    model.moveBalls();
    assertEquals(model.getBalls().get(0).getY(), 12, 0.5);
    assertEquals(model.getBalls().get(0).getX(), 12, 0);
    assertEquals(model.getBalls().get(0).getVelocity().y(), 0.25, 0);
    assertEquals(model.getBalls().get(0).getVelocity().x(), 0, 0);
    model.moveBalls();
    model.moveBalls();
    model.moveBalls();
    model.moveBalls();
    model.moveBalls();
    model.moveBalls();
    model.moveBalls();
    model.moveBalls();
    model.moveBalls();
    model.moveBalls();
    model.moveBalls();
    model.moveBalls();
    model.moveBalls();
    model.moveBalls();
    model.moveBalls();
    model.moveBalls();
    model.moveBalls();
    model.moveBalls();
    model.moveBalls();
    model.moveBalls();
    model.moveBalls();
    model.moveBalls();
    model.moveBalls();
    model.moveBalls();
    model.addKeyPress("key.32.down");
    model.addKeyPress("key.34.down");
    model.moveBalls();
    assertEquals(model.getBalls().get(0).getX(), 12, 0);
    assertEquals(model.getBalls().get(0).getY(), 12.8, 0);
    assertEquals(model.getBalls().get(0).getVelocity().x(), 0, 0);
    assertEquals(model.getBalls().get(0).getVelocity().y(), -5.9, 0.1);
    model.addKeyPress("asdf1");
    model.purgeKeyPresses();
    assertTrue(model.getKeyPresses().isEmpty());
  }

  @Test
  public void moveTest() throws badSelectException, DuplicateGizmoException {

    Gizmo simpleCircle = new CircleGizmo("2", 1, 1);
    model.addGizmo(simpleCircle, simpleCircle.getID());
    model.moveGizmo(simpleCircle.getID(), 2, 2);
    Gizmo simpleSquare = new Square("1", 12, 13);
    model.addGizmo(simpleSquare, simpleSquare.getID());
    model.moveGizmo(simpleSquare.getID(), 2, 2);


  }

  @Test
  public void connectionTest() throws badSelectException, DuplicateGizmoException, noSuchConnectionException {
    Gizmo simpleCircle = new CircleGizmo("2", 1, 1);
    model.addGizmo(simpleCircle, simpleCircle.getID());
    Gizmo simpleSquare = new Square("1", 12, 13);
    model.addGizmo(simpleSquare, simpleSquare.getID());
    model.addConnection("1", "2");
    assertTrue(simpleSquare.getGizmoConnections().contains("2"));
    model.removeConnection("1", "2");
    assertTrue(simpleSquare.getGizmoConnections().isEmpty());
  }

  @Test
  public void clearTest() throws badSelectException, DuplicateGizmoException, noSuchConnectionException {
    Gizmo simpleCircle = new CircleGizmo("2", 1, 1);
    model.addGizmo(simpleCircle, simpleCircle.getID());
    Gizmo simpleSquare = new Square("1", 12, 13);
    model.addGizmo(simpleSquare, simpleSquare.getID());
    model.addConnection("1", "2");
    model.addBall("BALL", 1, 1, 1, 1);
    model.clear();
    assertTrue(model.getGizmos().isEmpty());
  }


  @Test
  public void testModes() {
    model.setRotateMode(true);
    assertTrue(model.getRotateMode());
    model.setSelectedMode(true);
    assertTrue(model.getSelectMode());
    model.setMoveMode(true);
    assertTrue(model.getMoveMode());
    model.setConnectMode(true);
    assertTrue(model.getConnectMode());
    model.setDisconnectMode(true);
    assertTrue(model.getDisconnectMode());
    model.setDeleteMode(true);
    assertTrue(model.getDeleteMode());
    model.setKeyConnectMode(true);
    assertTrue(model.getKeyConnectMode());
    model.setKeyDisconnectMode(true);
    assertTrue(model.getKeyDisconnectMode());
    model.setAddBallMode(true);
    assertTrue(model.getAddBallMode());
    model.setAddGizmoMode(true);
    assertTrue(model.getAddGizmoMode());
  }

  @Test
  public void AbsorberTest() throws badSelectException, DuplicateGizmoException, noSuchConnectionException {
    Absorber abs = new Absorber("A", 1, 1, 20, 20);
    abs.setVelocityToFireBall(1);
    model.addGizmo(abs, abs.getID());
    model.addBall("BALLONE", 1, 0, 0, 0);
    model.addConnection(abs.getID(), abs.getID());
    model.getBalls().get(0).start();
    for (int i = 0; i < 30; i++) {
      model.moveBalls();
    }
    assertEquals(model.getBalls().get(0).getX(), 1.0, 0.01);
    assertEquals(model.getBalls().get(0).getY(), 0.8, 0.1);
    abs.addKeyConnect("32", "up");
    assertEquals(abs.toString(), "Absorber A 1 1 20 20\n" +
        "KeyConnect key 32 up A\n" +
        "Connect A A");


    assertEquals(abs.getKeyConnections().get(0), "key.32.up");
    abs.removeKeyConnects();
    assertTrue(abs.getKeyConnections().isEmpty());

    //Don't do anything, for coverage.
    abs.getBoardX();
    abs.getBoardY();

    abs.move(0, 0);
    assertTrue(abs.getGizmoConnections().contains(abs.getID()));
    assertFalse(abs.addGizmoConnection(abs.getID()));
    abs.removeConnection(abs.getID());
  }

  @Test
  public void ballTest() {
    model.addBall("BALLONE", 1, 0, 0, 0);
    model.stopAllBalls();
    assertEquals(model.getBalls().get(0).toString(), "Ball BALLONE 1.0 0.0 0.0 0.0");
    assertEquals(model.getBalls().get(0).hashCode(), 379125735);

  }

  @Test
  public void circleTest() throws badSelectException, DuplicateGizmoException, noSuchConnectionException {
    Gizmo simpleCircle = new CircleGizmo("2", 1, 1);
    model.addGizmo(simpleCircle, simpleCircle.getID());


    Gizmo simpleSquare = new Square("1", 12, 13);
    model.addGizmo(simpleSquare, simpleSquare.getID());
    model.addConnection("2", "1");


    simpleCircle.addKeyConnect("32", "down");

    assertEquals(simpleCircle.toString(), "Circle  2 1 1\n" +
        "KeyConnect key 32 down 2\n" +
        "Connect 2 1");

    model.moveGizmo("2", 5, 5);
    assertEquals(simpleCircle.getX(), 5);
    assertEquals(simpleCircle.getY(), 5);
    assertTrue(simpleCircle.getLineSegments().isEmpty());
    assertEquals(simpleCircle.getColor(), Color.green);
    assertEquals(simpleCircle.getType(), "Circle");
    assertTrue(simpleCircle.getCircles().contains(((CircleGizmo) simpleCircle).getCircle()));
    simpleCircle.trigger();
    assertEquals(simpleCircle.getColor(), Color.yellow);

    assertEquals(simpleCircle.hashCode(), 50);
    assertFalse(simpleCircle.equals(simpleSquare));

  }

  @Test
  public void leftFlipperTest() throws badSelectException, DuplicateGizmoException, noSuchConnectionException {
    LeftFlipper simpleLeft = new LeftFlipper("2", 1, 1);
    model.addGizmo(simpleLeft, "2");


    Gizmo simpleSquare = new Square("1", 12, 13);
    model.addGizmo(simpleSquare, simpleSquare.getID());
    model.addConnection("2", "1");
    assertTrue(simpleLeft.getGizmoConnections().contains("1"));
    //Pain in the neck someone else do it angryface
    model.moveGizmo(simpleLeft.getID(),2,2);
    assertEquals(simpleLeft.getX(),2);
    assertEquals(simpleLeft.getY(),2);
    model.addKeyConnect(simpleLeft.getID(),"32","down");
    simpleLeft.rotate();
    System.out.println(simpleLeft.toString());
    assertEquals(simpleLeft.toString(),"LeftFlipper 2 2 2\n" +
        "Rotate 2\n" +
        "KeyConnect key 32 down 2\n" +
        "Connect 2 1");
    simpleLeft.stateSwap();


  }

  @Test
  public void SquareTest() throws badSelectException, DuplicateGizmoException, noSuchConnectionException {
    Gizmo simpleCircle = new CircleGizmo("2", 1, 1);
    model.addGizmo(simpleCircle, simpleCircle.getID());

    Gizmo simpleSquare = new Square("1", 12, 13);
    model.addGizmo(simpleSquare, simpleSquare.getID());
    model.addConnection("1", "2");
    model.addKeyConnect("1","32","down");
   assertEquals(simpleSquare.toString(),"Square  1 12 13\n" +
       "KeyConnect key 32 down 1\n" +
       "Connect 1 2");
    simpleSquare.getBoardX();
    simpleSquare.getBoardY();
  }

  @Test
  public void triangleTest() throws badSelectException, DuplicateGizmoException, noSuchConnectionException {
    Gizmo simpleTriangle = new Triangle("2", 1, 1);
    model.addGizmo(simpleTriangle, "2");

    Gizmo simpleSquare = new Square("1", 12, 13);
    model.addGizmo(simpleSquare, simpleSquare.getID());

    model.addConnection("2", "1");
    model.addKeyConnect("2","32","down");
    assertEquals(simpleTriangle.toString(),"Triangle 2 1 1\n" +
        "KeyConnect key 32 down 2\n" +
        "Connect 2 1");
    model.moveGizmo("2",3,3);
    assertEquals(simpleTriangle.getX(),3);
    assertEquals(simpleTriangle.getY(),3);
    assertTrue(simpleTriangle.getGizmoConnections().contains("1"));

    int[] x = simpleTriangle.getBoardX();
    int[] y = simpleTriangle.getBoardY();
    assertEquals(x[0],3);
    assertEquals(x[1],3);
    assertEquals(x[2],4);

    assertEquals(y[0],3);
    assertEquals(y[1],4);
    assertEquals(y[2],3);

    simpleTriangle.rotate();

    x = simpleTriangle.getBoardX();
    y = simpleTriangle.getBoardY();
    assertEquals(x[0],3);
    assertEquals(x[1],4);
    assertEquals(x[2],4);

    assertEquals(y[0],3);
    assertEquals(y[1],3);
    assertEquals(y[2],4);


    simpleTriangle.rotate();

    x = simpleTriangle.getBoardX();
    y = simpleTriangle.getBoardY();
    assertEquals(x[0],4);
    assertEquals(x[1],3);
    assertEquals(x[2],4);

    assertEquals(y[0],3);
    assertEquals(y[1],4);
    assertEquals(y[2],4);


    simpleTriangle.rotate();


    x = simpleTriangle.getBoardX();
    y = simpleTriangle.getBoardY();
    assertEquals(x[0],3);
    assertEquals(x[1],3);
    assertEquals(x[2],4);

    assertEquals(y[0],3);
    assertEquals(y[1],4);
    assertEquals(y[2],4);

    assertEquals(simpleTriangle.getColor(), Color.blue);
    simpleTriangle.trigger();
    assertEquals(simpleTriangle.getColor(), Color.cyan);

    simpleTriangle.removeConnection("1");
    assertTrue(simpleTriangle.getGizmoConnections().isEmpty());

  }

  @Test
  public void rightFlipperTest() throws badSelectException, DuplicateGizmoException, noSuchConnectionException {
    RightFlipper simpleRight = new RightFlipper("2", 1, 1);
    model.addGizmo(simpleRight, "2");

    Gizmo simpleSquare = new Square("1", 12, 13);
    model.addGizmo(simpleSquare, simpleSquare.getID());
    model.addConnection("2", "1");
    assertTrue(simpleRight.getGizmoConnections().contains("1"));
    model.moveGizmo(simpleRight.getID(),2,2);
    assertEquals(simpleRight.getX(),2);
    assertEquals(simpleRight.getY(),2);
    model.addKeyConnect(simpleRight.getID(),"32","down");
    simpleRight.rotate();
    System.out.println(simpleRight.toString());
    assertEquals(simpleRight.toString(),"RightFlipper 2 2 2\n" +
            "KeyConnect key 32 down 2\n" +
            "Connect 2 1");
    simpleRight.stateSwap();


  }

  @Test
  public void teleporterTest() throws badSelectException, DuplicateGizmoException, noSuchConnectionException {
    TelePorter simpleTeleporter = new TelePorter("2", 1, 1);
    model.addGizmo(simpleTeleporter, "2");

    Gizmo simpleSquare = new Square("1", 7, 7);
    model.addGizmo(simpleSquare, simpleSquare.getID());
    model.addConnection("2", "1");
    assertTrue(simpleTeleporter.getGizmoConnections().contains("1"));
    model.moveGizmo(simpleTeleporter.getID(),2,2);
    assertEquals(simpleTeleporter.getX(),2);
    assertEquals(simpleTeleporter.getY(),2);
    model.addKeyConnect(simpleTeleporter.getID(),"32","down");
    assertEquals(simpleTeleporter.toString(),"Teleporter 2 2 2\n" +
            "KeyConnect key 32 down 2\n" +
            "Connect 2 1");
    simpleTeleporter.setColor(Color.black);
    assertTrue(simpleTeleporter.equals(simpleTeleporter));
    simpleTeleporter.trigger();
  }

}
