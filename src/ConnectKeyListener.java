import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class ConnectKeyListener implements KeyListener {
    private JDialog jDialog;
    private fakeModel model;
    private String keyID;
    private JLabel infolABEL;
    private ButtonGroup  buttonGroup;
    public ConnectKeyListener(JDialog jDialog, fakeModel model, String keyID, JLabel infolABEL, ButtonGroup buttonGroup) {
        this.jDialog = jDialog;
        this.model = model;
        this.keyID = keyID;
        this.infolABEL = infolABEL;
        this.buttonGroup = buttonGroup;
    }
    @Override
    public void keyTyped(KeyEvent keyEvent) {

    }

    @Override
    public void keyPressed(KeyEvent keyEvent) {

    }

    @Override
    public void keyReleased(KeyEvent keyEvent) {
        String key = Integer.toString(keyEvent.getKeyCode());
        System.out.println(key);
        ButtonModel selection = buttonGroup.getSelection();
        System.out.println(selection.getActionCommand());
        if (selection.getActionCommand().equalsIgnoreCase("key down")) {
           if(!model.addKeyConnect(keyID, key, "down")){
               infolABEL.setText("Duplicate keyconnect!");
               infolABEL.setBorder(BorderFactory.createBevelBorder(1));
               infolABEL.setBackground(Color.red);
               jDialog.dispose();
           }
           else{
               infolABEL.setText("Key " + key + " Added to gizmo " + keyID);
               infolABEL.setBorder(BorderFactory.createBevelBorder(1));
               infolABEL.setBackground(Color.green);
               jDialog.dispose();
           }
        } else if (selection.getActionCommand().equalsIgnoreCase("key up")) {
            if(!model.addKeyConnect(keyID, key, "up")){
                infolABEL.setText("Duplicate keyconnect!");
                infolABEL.setBorder(BorderFactory.createBevelBorder(1));
                infolABEL.setBackground(Color.red);
                jDialog.dispose();
            }
            else{
                infolABEL.setText("Key " + key + " Added to gizmo " + keyID);
                infolABEL.setBorder(BorderFactory.createBevelBorder(1));
                infolABEL.setBackground(Color.green);
                jDialog.dispose();
            }
        }
    }
}
